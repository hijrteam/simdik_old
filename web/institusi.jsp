<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	
	<div class="container">
	
	
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title pull-left">Daftar Institusi</h3>
		            <div class="btn-toolbar">
						<button id="btnRefresh" data-id="1" class="btn btn-default pull-right">
					        <span class="glyphicon glyphicon-refresh"></span>
				        </button>
						<button id="btnAdd" class="btn btn-default pull-right">
					        <span class="glyphicon glyphicon-plus"></span>
				        </button>
		        
		        </div>
		        <div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8">
						<div class="input-group">
					      <div class="input-group-btn">
					        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="column" data-id="">Kriteria Pencarian</span> <span class="caret"></span></button>
					        <ul class="dropdown-menu">
					          <li><a href="#" onclick="changeFilter('nama_institusi','Nama Pengguna')">Nama Institusi</a></li>
					          <li><a href="#" onclick="changeFilter('nama_jenjang','Nama Pengguna')">Jenjang</a></li>
					          <li role="separator" class="divider"></li>
	          				  <li><a href="#" onclick="changeFilter('','')">Reset Pencarian</a></li>
					        </ul>
					      </div><!-- /btn-group -->
					      <input id="txtsearch" type="text" class="form-control" aria-label="..." placeholder="Ketik kata kunci yang ingin anda cari">
					      <span class="input-group-btn">
					        <button id="btnSearch" onclick="search()" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-search"></span> Cari
							</button>
					      </span>
					      
					    </div><!-- /input-group -->
					  
					
					</div>
					<div class="col-md-4">
						
					    	<nav>
							<div class="pull-right">
								<span>Halaman: </span>
								<ul id="tblpages" class="pagination" style="margin: 0px !important; vertical-align: middle;"></ul>
								</div>	
							</nav>
					</div>
				</div>
				<br/>
				
				<table id="tbl" class="table">
					    <thead>
								<tr class="primary">
									<th width="20">No</th>
									<th width="100">Nama Institusi</th>
									<th width="100">Jenjang</th>
									<th width="120">Keterangan</th>
									<th width="80">Action</th>
								</tr>
							</thead>
							
							<tbody>
							
							</tbody>
					  	</table>
					  	
						
				
			</div>
			
						
			
		</div>
	</div>
	
	
	
	<!-- Modal Feature Begin -->
	<form id="frmInput" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modalInput" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Form Institusi</h4>
					
				</div>
				
				<div class="modal-body">
					 <div  id="progressBar"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
					<div class="row">
				      <div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
				      </div>
		    		</div>
						<p class="text-danger">Input dengan tanda (*) wajib diisi.</p>
					
				
					<input type="text" id="id" value="0" />
			    	
					<div class="form-group">
				      <div class="col-md-6">
				      	<label>Nama Institusi *</label>
			    			<input autocomplete="off" type="text" class="form-control" id="namaInstitusi" placeholder="Ketik nama institusi anda">
				      </div>
				      <div class="col-md-6">
				      	<label>Alamat Institusi *</label>
			    			<input autocomplete="off" type="text" class="form-control" id="alamat" placeholder="Ketik alamat anda">
				      </div>
				    </div>
				    
					<div class="form-group">
				      <div class="col-md-6">
				      	<label>Kode Pos *</label>
			    			<input autocomplete="off" type="text" class="form-control" id="kodePos" placeholder="Ketik kode pos anda">
				      </div>
				      <div class="col-md-6">
				      	<label>Nomor Telepon *</label>
			    			<input autocomplete="off" type="text" class="form-control" id="nomorTelepon" placeholder="Ketik nomor telepon anda">
				      </div>
				    </div>
				    
					<div class="form-group">
				      <div class="col-md-6">
				      	<label>Nomor Faks </label>
			    			<input autocomplete="off" type="text" class="form-control" id="nomorFaks" placeholder="Ketik nomor faks anda">
				      </div>
				      <div class="col-md-6">
				      	<label>Email </label>
			    			<input autocomplete="off" type="email" class="form-control" id="email" placeholder="Ketik email anda">
				      </div>
				    </div>
				    
				    <div class="form-group">
				      <div class="col-md-12">
				      	<label>Jenjang *</label>
			    			<select class="form-control" id="jenjang">
			    				<option value="0">-- Pilih Jenjang --</option>
							</select>
				      </div>				      
				    </div>
				    
				   	<div class="form-group">
				      <div class="col-md-12">
				      	<label>Keterangan</label>
			    			<textarea id="keterangan" class="form-control" rows="3"></textarea>
				      </div>
				    </div>
			    
			    </div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btnReset" type="button" class="btn btn-default" aria-hidden="true">RESET</button>
					<button id="btnSave" type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-floppy-disk"></span> 
					SIMPAN</button>
				</div>
			</div>
			</div>
		</div>
	</form>
	
	<!-- Modal Feature-->
	<div class="modal fade" data-backdrop="static" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
		<div class="modal-dialog">
			
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
					<div class="row">
						<div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
						</div>
					</div>
				</div>
				
				<div class="modal-body">		
					<div  id="progressBarRemove"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>		
					<div class="form-group">
						<p>Apakah anda yakin akan menghapus data pengguna ini?</p>
					</div>
					
					<div class="modal-footer">
						<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btnRemove" onclick="doRemove($(this).data('id'));" data-id="0" type="button" class="btn btn-primary">
						<span class="glyphicon glyphicon-trash"></span>  
						HAPUS</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	 
	 <script src="js/institusi-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>