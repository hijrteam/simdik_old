
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    $.backstretch("img/backgrounds/1.jpg");
    
    /*
        Form validation
    */
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.login-form').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    $('.login-form').submit(function( event ) {
    	
    	$('#logo').attr("src","img/hijr.png");
    	$('#btnLogin').text("Please wait..");
    	
		var obj = {
				user : {
					"username" : $("#form-username").val(),
					"password" : $.md5($("#form-password").val())
				}
			};
		
		var n = new Date().getTime();
		$.ajax({
	        url : "./rest/public/login?"+n,
	        type : "POST",
	        traditional : true,
	        contentType : "application/json",
	        dataType : "json",
	        data : JSON.stringify(obj),
	        success : function (response) {
	        	if (response.status == 'OK') {
					$.cookie("token", response.data.token, {});
					
					window.location="./";
				} else {
					$('#btnLogin').text("Sign In");
		        	$('#logo').attr("src","img/hijr-dis.png");
		        	
					alert(response.message);
				}
	        },
	        error : function (response) {
	        	$('#btnLogin').text("Sign In");
	        	$('#logo').attr("src","img/hijr-dis.png");
	        	
	        	alert("Connection error");
	        	
	        },
	    });
		event.preventDefault();
	});
    
    
});

