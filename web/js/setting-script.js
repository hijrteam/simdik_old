var url = "./rest/setting/";
var setting_data;

function init() {
	
	display();
	
	$('#btnSave').click(function(){
    	save();
    });
	
}

function save(){
	var lst = [];
	$( ".setting-field" ).each(function( index ) {
		
		  //console.log( $( this ).data('id') + ": " + $( this ).val() );
		  var setting = {
					
				"id" : $( this ).data('id'),
				"value" : $( this ).val()
			}
		  lst.push(setting);
	});
	var n = new Date().getTime();
	var obj = {"listSetting" : lst};
	$.ajax({
        url : url+"save?"+n,
        type : "POST",
        traditional : true,
        contentType : "application/json",
        dataType : "json",
        data : JSON.stringify(obj),
        success : function (response) {
        	if (response.status == 'OK') {
        		alert("Setting successfully saved");
			}else{
				alert("Save setting failed");
			}
        },
        error : function (response) {
        	alert("Connection error");
        },
    });
}

function getCategoryList(){
	var n = new Date().getTime();
	
	$.getJSON(url+'category?'+n, function(response) {
		if (response.status == 'OK') {
			var html = '';
			$.each(response.data, function(key, value) {
				html +=' <div class="panel-group">';
				html +='   <div class="panel panel-default">';
				html +='     <div class="panel-heading">';
				html +='       <h4 class="panel-title">';
				html +='         <a data-toggle="collapse" href="#collapse'+value.id+'">'+value.name+'</a>';
				html +='       </h4>';
				html +='     </div>';
				html +='     <div id="collapse'+value.id+'" class="panel-collapse collapse in">';
				html +='       <div class="panel-body">';
				$.each(setting_data, function(key1, value1) {
					if(value1.category.id==value.id){
						html +=' 		  <div class="form-group">';
						html +=' 		    <label class="control-label col-sm-4" for="'+value1.id+'">'+value1.name+':</label>';
						html +=' 		    <div class="col-sm-8">';
						html +=' 		      <input type="text" class="form-control setting-field" data-id="'+value1.id+'" value="'+value1.value+'">';
						html +=' 		    </div>';
						html +=' 		  </div>';
					}
				});
				html +=' 	</div>';
				html +='     </div>';
				html +='   </div>';
				html +=' </div>';
			});
			$('#setting').html(html);
		}else{
			alert("Connection error");
		}
	});
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(){

	var n = new Date().getTime();
	
	$.getJSON(url+"?"+n, function(response) {
		if (response.status == 'OK') {
			getCategoryList();
			setting_data = response.data;
			
		}else{
			alert("Connection error");
		}
	});
	
}

