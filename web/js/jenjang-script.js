var url = "./rest/jenjang/";
var urllist = "./rest/list/";
var jenjang;

function init() {
	
	display(1);
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
}


function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		display(1);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}


function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#modalInput").modal('show');
}

function validate(){
	var msg = '';
	
	if($('#namaJenjang').val() == ''){
		msg += 'Input masih belum lengkap';
	}
		
	return msg;
}

function save(){
	var msg = validate();
	
	if (msg == '') {
		
			cleanMessage('msg');
			$("#progressBar").show();

			var jenjang = {
				"id" : $('#id').val(),
				"namaJenjang" : $('#namaJenjang').val(),
				"keterangan" : $('#keterangan').val()
			}
			
			
			var obj = {"jenjang" : jenjang}
			// Basic update exclude password
			ajaxRequest(url + 'save',obj,'onSuccessSave','onErrorSave');
		
	}else{
		addAlert('msg', "alert-danger", msg);
	}
	
}

function onSuccessSave(response){
	$("#progressBar").hide();
	$("#modalInput").modal('hide');
	refresh();
	reset();
}


function onErrorSave(response){
	var msg = 'Gagal Simpan. ';
	if(response.message.indexOf('DUPLICATE') >= 0) {
		var errmsg = response.message.split(':');
		addAlert('msg', "alert-danger", msg + 'Duplikasi data input: `' + errmsg[1] + '`');
	}else{
		addAlert('msg', "alert-danger", msg + 'Error tidak diketahui');
	}
}


function reset(){
	cleanMessage('msg');
	$('#frmInput')[0].reset();
	
	$('#btnRemove').data("id","0");
	$("#progressBarRemove").hide();
	$("#modalconfirm").modal('hide');
	$("#progressBar").hide();
	$("#modalInput").modal('hide');
	
	jenjang = null;
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

function confirmRemove(id){
	// set data id to confirm dialog
	$("#btnRemove").data("id",id);
	$("#progressBarRemove").hide();
	$("#modalconfirm").modal('show');
}

function doRemove(id){
	
	$("#progressBarRemove").show();
	
	var obj = {
			"pagination" : {
				"filter": [{
					"column" : "jenjang_id",
					"operator" : "=",
					"value" : id
				}]
			}
	}
	
	ajaxRequest(url + 'delete',obj,'onSuccessRemove');
	
}

function onSuccessRemove(response){
	reset();
	refresh();
	alert('Data sudah dihapus.');
}



function edit(id){
	reset();
	$("#progressBar").show();
	$("#modalInput").modal('show');
	
	
	var obj = {
			"pagination" : {
				"rowcount" : 0,
				"activepage" : 1,
				"filter": [{
					"column" : "jenjang_id",
					"operator" : "=",
					"value" : id
				}]
			}
	}
	
	ajaxRequest(url,obj,'onSuccessEdit');
		
}

function onSuccessEdit(response){
	$("#progressBar").hide();
	jenjang = response.data[0];
	
	$('#id').val(jenjang.id);
	$('#namaJenjang').val(jenjang.namaJenjang);
	$('#keterangan').val(jenjang.keterangan);
	
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(page){

	var filter = [];
	
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
    	var clause = {
					"column" : $('#column').data('id'),
					"operator" : "like",
					"value" : $('#txtsearch').val()
				};
    	
    	filter.push(clause);
	}
	
	var obj = {
			"pagination" : {
				"rowcount" : 5,
				"activepage" : page,
				"filter": filter
			}
	}
	
	
	$('#btnRefresh').data("id",page);
	
	var tbody = $('#tbl').find($('tbody'));
	tbody.text('');
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	
	ajaxRequest(url,obj,'onSuccessDisplay');
			
}

function onSuccessDisplay(response){
	var tbody = $('#tbl').find($('tbody'));
	tbody.text('');
	
	
	var row = '';
	var i = 1;
	
	$.each(response.data, function(key, value) {
		
		row += '<tr>';
		row += '<td scope="row">'+value.rowNum+'</td>';
		row += '<td>'+value.namaJenjang+'</td>';
		row += '<td>'+value.keterangan+'</td>';
		
		row += '<td><button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
		row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
		row += '</td>';
		
		row += '</tr>';
		
		i++;
	});
	if(row == ''){
		tbody.text('');
		tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
	}else{
		tbody.html(row);
	}
	createPagination('tblpages','display', response.pagination);
}

