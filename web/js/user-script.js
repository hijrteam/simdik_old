var url = "./rest/user/";
var urllist = "./rest/list/";
var user;

function init() {
	
	display(1);
	getRoleList();
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		display(1);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}


function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function getRoleList(){
	var n = new Date().getTime();
	$.getJSON(urllist+'role/'+"?"+n, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("#role").append(new Option(value.name, value.id));
			});
		}else{
			alert("Connection error");
		}
	});
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#passwordForm").show();
	$("#modalInput").modal('show');
}

function validate(){
	var msg = '';
	
	if($('#btnUpdate').data("id") > 0){
		if($('#password1').val() == '' || $('#repassword1').val() == ''){
			msg = 'Kata kunci tidak boleh kosong';
		}
		
		if($('#password1').val() != $('#repassword1').val()){
			msg = 'Ketikan ulang kata kunci tidak sama <br />';
		}
		
	}else{
		
		if($('#password').val() != $('#repassword').val()){
			msg = 'Ketikan ulang kata kunci tidak sama <br />';
		}
		
		// if save existing record (EDIT/UPDATE)
		if ($('#id').val() == '0'){
			if($('#role').val() == '0' || $('#fullname').val() == '' || $('#username').val() == '' || $('#password').val() == '' || $('#repassword').val() == ''){
				msg += 'Input masih belum lengkap';
			}
		}else{
			if($('#role').val() == '0' || $('#fullname').val() == '' || $('#username').val() == ''){
				msg += 'Input masih belum lengkap';
			}
		}
	}
	
	
	return msg;
}

function save(){
	var msg = validate();
	
	if (msg == '') {
		// check which dialog opening
		if($('#btnUpdate').data("id") > 0){
			cleanMessage('msg1');
			$("#progressBar2").show();
			
			var obj = {
					"user" : {
						"id" : $('#btnUpdate').data("id"),
						"password" : $.md5($('#password1').val()),
					}
			}
			ajaxRequest(url + 'save',obj,'onSuccessPassword','onErrorPassword');
		}else{
			cleanMessage('msg');
			$("#progressBar").show();
			
			// make sure user set to null in reset function
			if(user != null) { // edit and update
				user.name = $('#fullname').val();
				user.email = $('#email').val();
				user.password = "";
				user.role.id = $('#role').val();
				user.username = $('#username').val();
				user.description = $('#description').val();
			}else{ // add new
				user = {
					"id" : $('#id').val(),
					"name" : $('#fullname').val(),
					"password" : $('#password').val(),
					"username" : $('#username').val(),
					"email" : $('#email').val(),
					"role" : {
						"id": $('#role').val()
					},
					"description" : $('#description').val()
				}
			}
			
			var obj = {"user" : user}
			// Basic update exclude password
			ajaxRequest(url + 'save',obj,'onSuccessSave','onErrorSave');
		}
	}else{
		if($('#btnUpdate').data("id") > 0){
			addAlert('msg1', "alert-danger", msg);
		}else{
			addAlert('msg', "alert-danger", msg);
		}
	}
	
}

function onSuccessSave(response){
	$("#progressBar").hide();
	$("#modalInput").modal('hide');
	refresh();
	reset();
}


function onErrorSave(response){
	var msg = 'Gagal Simpan. ';
	if(response.message.indexOf('DUPLICATE') >= 0) {
		var errmsg = response.message.split(':');
		addAlert('msg', "alert-danger", msg + 'Duplikasi data input: `' + errmsg[1] + '`');
	}else{
		addAlert('msg', "alert-danger", msg + 'Error tidak diketahui');
	}
}

function onSuccessPassword(response){	
	$("#progressBar2").hide();
	$("#modalPassword").modal('hide');
	reset();
	alert('Update password berhasil.');
}


function onErrorPassword(response){
	addAlert('msg1', "alert-danger", 'Gagal Simpan. Error tidak diketahui');
}

function reset(){
	cleanMessage('msg');
	
	$('#id').val('0');
	$('#fullname').val('');
	$('#username').val('');
	$('#password').val('');
	$('#repassword').val('');
	$('#password1').val('');
	$('#repassword1').val('');
	$('#role').val('0');
	$('#description').val('');
	$('#btnRemove').data("id","0");
	$('#btnUpdate').data("id","0");
	
	$("#progressBar1").hide();
	$("#modalconfirm").modal('hide');
	$("#progressBar2").hide();
	$("#modalPassword").modal('hide');
	$("#progressBar").hide();
	$("#modalInput").modal('hide');
	
	user = null;
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

function confirmRemove(id){
	// set data id to confirm dialog
	$("#btnRemove").data("id",id);
	$("#progressBar1").hide();
	$("#modalconfirm").modal('show');
}

function doRemove(id){
	
	$("#progressBar1").show();
	
	var obj = {
			"pagination" : {
				"filter": [{
					"column" : "user_id",
					"operator" : "=",
					"value" : id
				}]
			}
	}
	
	ajaxRequest(url + 'delete',obj,'onSuccessRemove');
	
}

function onSuccessRemove(response){
	reset();
	refresh();
	alert('Data sudah dihapus.');
}



function edit(id){
	reset();
	$("#progressBar").show();
	$("#passwordForm").hide();
	$("#modalInput").modal('show');
	
	
	var obj = {
			"pagination" : {
				"rowcount" : 0,
				"activepage" : 1,
				"filter": [{
					"column" : "user_id",
					"operator" : "=",
					"value" : id
				}]
			}
	}
	
	ajaxRequest(url,obj,'onSuccessEdit');
		
}

function onSuccessEdit(response){
	$("#progressBar").hide();
	user = response.data[0];
	$('#id').val(user.id);
	$('#fullname').val(user.name);
	$('#username').val(user.username);
	$('#role').val(user.role.id);
	$('#description').val(user.description);
}

function editPassword(id){
	$("#progressBar2").hide();	
	$('#btnUpdate').data("id",id);
	$("#modalPassword").modal('show');
}


/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(page){

	var filter = [];
	
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
    	var clause = {
					"column" : $('#column').data('id'),
					"operator" : "like",
					"value" : $('#txtsearch').val()
				};
    	
    	filter.push(clause);
	}
	
	var obj = {
			"pagination" : {
				"rowcount" : 5,
				"activepage" : page,
				"filter": filter
			}
	}
	
	
	$('#btnRefresh').data("id",page);
	
	var tbody = $('#tbl').find($('tbody'));
	tbody.text('');
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	
	ajaxRequest(url,obj,'onSuccessDisplay');
			
}

function onSuccessDisplay(response){
	var tbody = $('#tbl').find($('tbody'));
	tbody.text('');
	
	
	var row = '';
	var i = 1;
	
	$.each(response.data, function(key, value) {
		
		row += '<tr>';
		row += '<td scope="row">'+value.rowNum+'</td>';
		row += '<td>'+value.name+'</td>';
		row += '<td>'+value.username+'</td>';
		row += '<td>'+value.role.name+'</td>';
		row += '<td>'+value.email+'</td>';
		row += '<td>'+value.description+'</td>';
		
		row += '<td><button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
		
		row += '<button onclick="editPassword(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></button> ';
		
		row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
		row += '</td>';
		
		row += '</tr>';
		
		i++;
	});
	if(row == ''){
		tbody.text('');
		tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
	}else{
		tbody.html(row);
	}
	createPagination('tblpages','display', response.pagination);
}

