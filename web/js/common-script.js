var token;

jQuery(document).ready(function() {
	token = $.cookie("token");
	
	/*
	 * Format Input Type Date
	 */
    $(function() {
		$( ".datepicker" ).datepicker({
	      showOtherMonths: true,
	      selectOtherMonths: true,
	      dateFormat : "yy-mm-dd"
		});
		
	});
    
    
    
    /*
     * Format Input Type Currency
     */
    $('.currency').number( true,0);
    
    
    $('#frmChangePassword').submit(function(e){
    	doChangePassword();
		e.preventDefault();
	});
    
    
    init();
    
    //ajaxRequest('./rest/user/save',obj,'test');
});

function deferror(response){
	alert(response.status + ': '+ response.message);
	if(response.status == 'UNAUTHORIZED'){
		redirectLogin();
	}
}

function redirectLogin(){
	window.location='./login.html';
}

function logout(){
	
	var n = new Date().getTime();
	$.getJSON("./rest/"+username+"/logout?"+n, function(response) {
		if (response.status == 'OK') {
			$.removeCookie("token");
			redirectLogin();
		}else{
			alert("Connection error");
		}
	});
}

function ajaxRequest(url, obj, fnsuccess, fnerror){
	var n = new Date().getTime();
	console.log(JSON.stringify(obj));
	$.ajax({
	    url : url+"?"+n,
	    type : "POST",
	    traditional : true,
	    contentType : "application/json",
	    dataType : "json",
	    data : JSON.stringify(obj),
	    success : function (response) {
	    	if (response.status == 'OK') {
	    		var fn = window[fnsuccess];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}
			}else{
				var fn = window[fnerror];
	    		if(typeof fn === 'function') {
	    		    fn(response);
	    		}else{
	    			deferror(response);
	    		}
			}
	    },
	    error : function (response) {
	    	alert("Connection error");
	    },
	});
}

function showChangePassword(){
	resetChangePassword();
	$("#changePasswordProgress").hide();
	$('#modalChangePassword').modal('show');
	
	$('#modalChangePassword').on('shown.bs.modal', function() {
		$('#changePasswordOld').focus();
    });
}


function resetChangePassword(){
	$('#changePasswordNew').val('');
	$('#changePasswordNewRetype').val('');
	$('#changePasswordOld').val('');
}

function doChangePassword(){
	var msg = '';
	
	if($('#changePasswordNew').val() == '' || $('#changePasswordNewRetype').val() == ''){
		msg = 'Kata kunci tidak boleh kosong';
	}
	
	if($('#changePasswordNew').val() != $('#changePasswordNewRetype').val()){
		msg = 'Ketikan ulang kata kunci tidak cocok <br />';
	}
	
	if (msg == '') {
		$("#changePasswordProgress").show();
		
		var obj = {
				"user" :  {		
					"password" :$.md5($('#changePasswordNew').val()),
					"currentPassword" :$.md5($('#changePasswordOld').val())
				}
		} ;
	
		
		var n = new Date().getTime();
		$.ajax({
	        url : "./rest/"+username+"/changePassword?"+n,
	        type : "POST",
	        traditional : true,
	        contentType : "application/json",
	        dataType : "json",
	        data : JSON.stringify(obj),
	        success : function (response) {
	        	if (response.status == 'OK') {
	        		$("#modalChangePassword").modal('hide');
	        		resetChangePassword();
				}else{
					if(response.message.match("^UNMATCHPASSWD")){
	        			addAlert('changePasswordMsg', "alert-danger", "Your current password does not match");
	        		}else{
	        			addAlert('changePasswordMsg', "alert-danger", 'Gagal Simpan. Error tidak diketahui');
	        		}
				}
	        	$("#changePasswordProgress").hide();
	        },
	        error : function (response) {
	        	alert("Connection error");
	        },
	    });
		
		
	}else{
		addAlert('changePasswordMsg', "alert-danger", msg);
	}
}

function cleanMessage(id){
	$('#'+id).hide();
	$('#'+id).removeClass("alert-success");
	$('#'+id).removeClass("alert-danger");
	$('#'+id).text('');
}

function addAlert(id, type, message) {
	
	cleanMessage(id);
	
	$('#'+id).addClass(type);
	var premsg = '';
	if (type == 'alert-success') {
		premsg = '<strong>Success!</strong><br />';

	}
	if (type == 'alert-danger') {
		premsg = '<strong>Error!</strong><br />';
	}
	$('#'+id).append(premsg + message);
	$('#'+id).show();
}


/**
 * Create HTML links of available pages according to data list querying from database
 * @param id Element ID as container of pagination links
 * @param func Function to retrieve and display data
 * @param pagination Pagination object retrieve from ResponseWrapper
 */
function createPagination(id, func, pagination) {
	var html = '';
	$('#'+id).text('');

	if (pagination.rowcount == 0) {
		html += '<li class="disabled"><span aria-hidden="true">&laquo;</span> </li>';
		html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
	} else {
		if (pagination.activepage == 1) {
			html += '<li class="disabled"><span aria-hidden="true">&laquo;</span></li>';
		} else {
			html += '<li><a href="javascript:'+func+'('+(pagination.activepage - 1)+')" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a></li>';
		}
		var i;
		for ( i = 1; i <= pagination.pagecount; i++) {

			if (pagination.activepage == i) {
				html += '<li class="active"><a href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			} else {
				html += '<li><a href="javascript:'+func+'(' + i + ')">' + i + '</a></li>';
			}
		}
		if (pagination.activepage == pagination.pagecount) {
			html += '<li class="disabled"><span aria-hidden="true">&raquo;</span></li>';
		} else {
			html += '<li><a href="javascript:'+func+'(' +(pagination.activepage + 1) + ')" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
		}
	}

	$('#'+id).append(html);

}


