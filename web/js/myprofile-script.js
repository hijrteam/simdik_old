var urlme = "./rest/me/";
var urllist = "./rest/list/";
var urlUpload = "./rest/uploadFile/";

var list_marital_status = false;
var list_province = false;

function init() {
	
	$('#modalProgress').modal('show');
	getMaritalStatusList();
	getProvinceList();
	
	$('.calendar').click(function(){
    	if($( ".datepicker" ).data('datepicker') != null)
    		$( ".datepicker" ).datepicker('show');
    });
	

	$("#btnReset").click(function(e){
		reset();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$("#btnBrowse").click(function() {
		if($("#btnBrowse").text() == 'Pilih') {
			$("#uploadFile").click();
		}else{
			removeFile();
		}	
	});
}

function finishLoading(){
	if (list_marital_status == true && list_province == true){
		edit();
		
	}
}


function removeFile(){
	$("#fileName").val('');
	$('#uploadFile').data("id",'0');
	$("#btnBrowse").text('Pilih');
}

function readUrl() {
	var x = document.getElementById("uploadFile");
	var file = x.files[0];
    if ('name' in file) {
        $("#fileName").val(file.name);
        ajaxUpload('uploadFile','payment');
    }
}

function ajaxUpload(id, folder) {
	var data = new FormData();
	
	jQuery.each(jQuery('#'+id)[0].files, function(i, file) {
	    data.append('file-'+i, file);
	});
	$("#btnBrowse").text('Proses..');
	data.append('folder', folder);
	jQuery.ajax({
	    url: urlUpload+"save"+"?token="+token,
	    data: data,
	    cache: false,
	    contentType: false,
	    processData: false,
	    type: 'POST',
	    success: function(response){
	    	if (response.status == 'OK') {
	    		$("#btnBrowse").text('Hapus');
	    		$('#'+id).data("id",response.data.id)
	    		
	    		console.log('OK! ' + $('#'+id).data('id'));
	    	}else {
	    		alert('Upload file error!');
	    		removeFile();
	    	}
	    },
        error : function (response) {
        	alert('Connection error!');
        	removeFile();
        },
    
	});
	return false
}

function getMaritalStatusList(){
	var n = new Date().getTime();
	$.getJSON(urllist+'maritalstatus/'+"?"+n, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				var htmlType = "";
        		$.each(response.data, function(key, value) {
        			htmlType += '<label class="radio-inline">' +
        			'<input type="radio" name="marital_status" value="'+value.id+'">'+
        			value.name+'</label>';
        		});
        		$("#marital_status").html(htmlType);
			});
			list_marital_status = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}

function getProvinceList(){
	var n = new Date().getTime();
	$.getJSON(urllist+'province/'+"?"+n, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("#home_province").append(new Option(value.name, value.id));
			});
			list_province = true;
			finishLoading();
		}else{
			alert("Connection error");
		}
	});
}



function validate(){
	var msg = '';
	
	
		
	return msg;
}

function save(){
	var msg = validate();
	
	if (msg == '') {
		
			cleanMessage('msg');
			$('#modalProgress').modal('show');

			var profile = {
				"birthDate" : $('#birth_date').val(),
				"gender" : $("input:radio[name ='gender']:checked").val(),
				"maritalStatus" : {
					"id" : $("input:radio[name ='marital_status']:checked").val()
				},
				"homeAddress" : $('#home_address').val(),
				"homeCity" : $('#home_city').val(),
				"homePostalCode" : $('#home_postal_code').val(),
				"homePhoneNumber" : $('#home_phone_number').val(),
				"homeProvince" : {
					"id" : $("#home_province").val()
				},
				"mobilePhone" : $('#mobile_phone').val(),
				"picture" : {
					"id": $('#uploadFile').data("id")
				}
			}
			
			
			var obj = {"profile" : profile}
			// Basic update exclude password
			ajaxRequest(urlme + 'saveProfile',obj,'onSuccessSave','onErrorSave');
		
	}else{
		addAlert('msg', "alert-danger", msg);
	}
	
}

function onSuccessSave(response){
	$('#modalProgress').modal('hide');
	alert('Your profile successfully updated.');
}


function onErrorSave(response){
	var msg = 'Gagal Simpan. ';
	if(response.message.indexOf('DUPLICATE') >= 0) {
		var errmsg = response.message.split(':');
		addAlert('msg', "alert-danger", msg + 'Duplikasi data input: `' + errmsg[1] + '`');
	}else{
		addAlert('msg', "alert-danger", msg + 'Error tidak diketahui');
	}
}


function reset(){
	cleanMessage('msg');
	$('#frmInput')[0].reset();
	
}


function edit(){
	reset();
	
	
	var n = new Date().getTime();
	$.getJSON(urlme+"profile?"+n, function(response) {
		if (response.status == 'OK') {
			var profile =response.data;
				$('#id').val(profile.id);
				console.log(profile);
				$('#user').data("id",profile.user.id);
				$('#user').val(profile.user.name);
				$('#birth_date').val(profile.birthDate);
				$('input:radio[name=gender][value='+profile.gender+']').click();
				$('input:radio[name=marital_status][value='+profile.maritalStatus.id+']').click();
				$('#home_address').val(profile.homeAddress);
				$('#home_city').val(profile.homeCity);
				$('#home_postal_code').val(profile.homePostalCode);
				$('#home_province').val(profile.homeProvince.id);
				$('#home_phone_number').val(profile.homePhoneNumber);
				$('#mobile_phone').val(profile.mobilePhone);
				$('#fileName').val(profile.picture.name);
				$('#uploadFile').data("id",profile.picture.id);
			$('#modalProgress').modal('hide');
		}else{
			alert("Connection error");
		}
	});
		
}
