var url = "./rest/profile/";
var urllist = "./rest/list/";
var urlUpload = "./rest/uploadFile/";
var profile;

function init() {
	
	display(1);
	getMaritalStatusList();
	getProvinceList();
	getUserList();
	
	$('.calendar').click(function(){
    	if($( ".datepicker" ).data('datepicker') != null)
    		$( ".datepicker" ).datepicker('show');
    });
	
	$("#btnAdd").click(function(e){
		add();
	});
	
	$("#btnReset").click(function(e){
		reset();
	});
	
	$("#btnRefresh").click(function(e){
		refresh();
	});
	
	$('#frmInput').submit(function(e){
		save();
		e.preventDefault();
	});
	
	$('#txtsearch').keypress(function(e){
	    if(e.keyCode == 13)
	    {
	        search();
	    }
	});
	
	$("#btnBrowse").click(function() {
		if($("#btnBrowse").text() == 'Pilih') {
			$("#uploadFile").click();
		}else{
			removeFile();
		}	
	});
}


function removeFile(){
	$("#fileName").val('');
	$('#uploadFile').data("id",'0');
	$("#btnBrowse").text('Pilih');
}

function readUrl() {
	var x = document.getElementById("uploadFile");
	var file = x.files[0];
    if ('name' in file) {
        $("#fileName").val(file.name);
        ajaxUpload('uploadFile','payment');
    }
}

function ajaxUpload(id, folder) {
	var data = new FormData();
	
	jQuery.each(jQuery('#'+id)[0].files, function(i, file) {
	    data.append('file-'+i, file);
	});
	$("#btnBrowse").text('Proses..');
	data.append('folder', folder);
	jQuery.ajax({
	    url: urlUpload+"save"+"?token="+token,
	    data: data,
	    cache: false,
	    contentType: false,
	    processData: false,
	    type: 'POST',
	    success: function(response){
	    	if (response.status == 'OK') {
	    		$("#btnBrowse").text('Hapus');
	    		$('#'+id).data("id",response.data.id)
	    		
	    		console.log('OK! ' + $('#'+id).data('id'));
	    	}else {
	    		alert('Upload file error!');
	    		removeFile();
	    	}
	    },
        error : function (response) {
        	alert('Connection error!');
        	removeFile();
        },
    
	});
	return false
}

function search(){
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
		display(1);
	}else{
		alert('Tentukan kriteria pencarian anda');
	}
}


function changeFilter(col, label){
	if (col == '') {
		label = 'Kriteria Pencarian';
		$('#txtsearch').val('');
	}
	$('#column').data('id', col);
	$('#column').text(label);
	$('#txtsearch').focus();
}

function getMaritalStatusList(){
	var n = new Date().getTime();
	$.getJSON(urllist+'maritalstatus/'+"?"+n, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				var htmlType = "";
        		$.each(response.data, function(key, value) {
        			htmlType += '<label class="radio-inline">' +
        			'<input type="radio" name="marital_status" value="'+value.id+'">'+
        			value.name+'</label>';
        		});
        		$("#marital_status").html(htmlType);
			});
		}else{
			alert("Connection error");
		}
	});
}

function getProvinceList(){
	var n = new Date().getTime();
	$.getJSON(urllist+'province/'+"?"+n, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("#home_province").append(new Option(value.name, value.id));
			});
		}else{
			alert("Connection error");
		}
	});
}

function getUserList(){
	var n = new Date().getTime();
	$.getJSON(urllist+'user/'+"?"+n, function(response) {
		if (response.status == 'OK') {
			$.each(response.data, function(key, value) {
				$("#user").append(new Option(value.name, value.id));
			});
		}else{
			alert("Connection error");
		}
	});
}

function add(){
	reset();
	$("#progressBar").hide();
	$("#passwordForm").show();
	$("#modalInput").modal('show');
}

function validate(){
	var msg = '';
	
		
	if($('#password').val() != $('#repassword').val()){
		msg = 'Ketikan ulang kata kunci tidak sama <br />';
	}
	
	if($('#role').val() == '0' || $('#fullname').val() == '' || $('#profilename').val() == '' || $('#password').val() == '' || $('#repassword').val() == ''){
		msg += 'Input masih belum lengkap';
	}
		
	return msg;
}

function save(){
	var msg = validate();
	
	if (msg == '') {
		
			cleanMessage('msg');
			$("#progressBar").show();

			var profile = {
				"id" : $('#id').val(),
				"user" :{
					"id": $('#user').val()
				},
				"birthDate" : $('#birth_date').val(),
				"gender" : $("input:radio[name ='gender']:checked").val(),
				"maritalStatus" : {
					"id" : $("input:radio[name ='marital_status']:checked").val()
				},
				"homeAddress" : $('#home_address').val(),
				"homeCity" : $('#home_city').val(),
				"homePostalCode" : $('#home_postal_code').val(),
				"homePhoneNumber" : $('#home_phone_number').val(),
				"homeProvince" : {
					"id" : $("#home_province").val()
				},
				"mobilePhone" : $('#mobile_phone').val(),
				"picture" : {
					"id": $('#uploadFile').data("id")
				}
			}
			
			
			var obj = {"profile" : profile}
			// Basic update exclude password
			ajaxRequest(url + 'save',obj,'onSuccessSave','onErrorSave');
		
	}else{
		addAlert('msg', "alert-danger", msg);
	}
	
}

function onSuccessSave(response){
	$("#progressBar").hide();
	$("#modalInput").modal('hide');
	refresh();
	reset();
}


function onErrorSave(response){
	var msg = 'Gagal Simpan. ';
	if(response.message.indexOf('DUPLICATE') >= 0) {
		var errmsg = response.message.split(':');
		addAlert('msg', "alert-danger", msg + 'Duplikasi data input: `' + errmsg[1] + '`');
	}else{
		addAlert('msg', "alert-danger", msg + 'Error tidak diketahui');
	}
}


function reset(){
	cleanMessage('msg');
	$('#frmInput')[0].reset();
	
	$('#btnRemove').data("id","0");
	$("#progressBarRemove").hide();
	$("#modalconfirm").modal('hide');
	$("#progressBar").hide();
	$("#modalInput").modal('hide');
	
	profile = null;
}

function refresh(){
	var page = $('#btnRefresh').data("id");
	display(page);
}

function confirmRemove(id){
	// set data id to confirm dialog
	$("#btnRemove").data("id",id);
	$("#progressBarRemove").hide();
	$("#modalconfirm").modal('show');
}

function doRemove(id){
	
	$("#progressBarRemove").show();
	
	var obj = {
			"pagination" : {
				"filter": [{
					"column" : "profile_id",
					"operator" : "=",
					"value" : id
				}]
			}
	}
	
	ajaxRequest(url + 'delete',obj,'onSuccessRemove');
	
}

function onSuccessRemove(response){
	reset();
	refresh();
	alert('Data sudah dihapus.');
}



function edit(id){
	reset();
	$("#progressBar").show();
	$("#passwordForm").hide();
	$("#modalInput").modal('show');
	
	
	var obj = {
			"pagination" : {
				"rowcount" : 0,
				"activepage" : 1,
				"filter": [{
					"column" : "profile_id",
					"operator" : "=",
					"value" : id
				}]
			}
	}
	
	ajaxRequest(url,obj,'onSuccessEdit');
		
}

function onSuccessEdit(response){
	$("#progressBar").hide();
	profile = response.data[0];
	
	$('#id').val(profile.id);
	$('#user').val(profile.user.id);
	$('#birth_date').val(profile.birthDate);
	$('input:radio[name=gender][value='+profile.gender+']').click();
	$('input:radio[name=marital_status][value='+profile.maritalStatus.id+']').click();
	$('#home_address').val(profile.homeAddress);
	$('#home_city').val(profile.homeCity);
	$('#home_postal_code').val(profile.homePostalCode);
	$('#home_province').val(profile.homeProvince.id);
	$('#home_phone_number').val(profile.homePhoneNumber);
	$('#mobile_phone').val(profile.mobilePhone);
	$('#fileName').val(profile.picture.name);
	$('#uploadFile').data("id",profile.picture.id);
	
	
}

/**
 * Retrieve data and display at the certain page
 * @param id HTML table id
 * @param page Page number to display
 */
function display(page){

	var filter = [];
	
	if($('#txtsearch').val() != '' && $('#column').data('id') != ''){
    	var clause = {
					"column" : $('#column').data('id'),
					"operator" : "like",
					"value" : $('#txtsearch').val()
				};
    	
    	filter.push(clause);
	}
	
	var obj = {
			"pagination" : {
				"rowcount" : 5,
				"activepage" : page,
				"filter": filter
			}
	}
	
	
	$('#btnRefresh').data("id",page);
	
	var tbody = $('#tbl').find($('tbody'));
	tbody.text('');
	tbody.append('<tr><td colspan="10" align="center">Loading data....</td></tr>');
	
	ajaxRequest(url,obj,'onSuccessDisplay');
			
}

function onSuccessDisplay(response){
	var tbody = $('#tbl').find($('tbody'));
	tbody.text('');
	
	
	var row = '';
	var i = 1;
	
	$.each(response.data, function(key, value) {
		
		row += '<tr>';
		row += '<td scope="row">'+value.rowNum+'</td>';
		row += '<td>'+value.user.name+'</td>';
		row += '<td>'+value.homeAddress+' - '+value.homeCity+' - '+value.homeProvince.name+' - '+value.homePostalCode+'</td>';
		row += '<td>'+value.homePhoneNumber+' / '+value.mobilePhone+'</td>';
		row += '<td>'+value.birthDate+'</td>';
		if(value.gender == '1'){
			row += '<td>Pria</td>';
		}else{
			row += '<td>Wanita</td>';
		}
		row += '<td>'+value.maritalStatus.name+'</td>';
		
		row += '<td><button onclick="edit(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button> ';
		row += '<button onclick="confirmRemove(' + value.id + ')" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
		row += '</td>';
		
		row += '</tr>';
		
		i++;
	});
	if(row == ''){
		tbody.text('');
		tbody.append('<tr><td colspan="10" align="center">Data tidak tersedia</td></tr>');
	}else{
		tbody.html(row);
	}
	createPagination('tblpages','display', response.pagination);
}

