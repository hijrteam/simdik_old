<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	
	<div class="container">
	
	
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title pull-left">Daftar Profile Pengguna</h3>
		            <div class="btn-toolbar">
						<button id="btnRefresh" data-id="1" class="btn btn-default pull-right">
					        <span class="glyphicon glyphicon-refresh"></span>
				        </button>
						<button id="btnAdd" class="btn btn-default pull-right">
					        <span class="glyphicon glyphicon-plus"></span>
				        </button>
		        
		        </div>
		        <div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8">
						<div class="input-group">
					      <div class="input-group-btn">
					        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="column" data-id="">Kriteria Pencarian</span> <span class="caret"></span></button>
					        <ul class="dropdown-menu">
					          <li><a href="#" onclick="changeFilter('real_name','Nama Pengguna')">Nama Pengguna</a></li>
					          <li><a href="#" onclick="changeFilter('email','Email')">Email</a></li>
					          <li role="separator" class="divider"></li>
	          				  <li><a href="#" onclick="changeFilter('','')">Reset Pencarian</a></li>
					        </ul>
					      </div><!-- /btn-group -->
					      <input id="txtsearch" type="text" class="form-control" aria-label="..." placeholder="Ketik kata kunci yang ingin anda cari">
					      <span class="input-group-btn">
					        <button id="btnSearch" onclick="search()" type="button" class="btn btn-default">
								<span class="glyphicon glyphicon-search"></span> Cari
							</button>
					      </span>
					      
					    </div><!-- /input-group -->
					  
					
					</div>
					<div class="col-md-4">
						
					    	<nav>
							<div class="pull-right">
								<span>Halaman: </span>
								<ul id="tblpages" class="pagination" style="margin: 0px !important; vertical-align: middle;"></ul>
								</div>	
							</nav>
					</div>
				</div>
				<br/>
				
				<table id="tbl" class="table">
					    <thead>
								<tr class="primary">
									<th width="20">No</th>
									<th width="100">Nama Lengkap</th>
									<th width="150">Alamat Rumah</th>
									<th width="100">Telepon</th>
									<th width="80">Tanggal Lahir</th>
									<th width="80">Kelamin</th>
									<th width="80">Status</th>
									<th width="80">Action</th>
								</tr>
							</thead>
							
							<tbody>
							
							</tbody>
					  	</table>
					  	
						
				
			</div>
			
						
			
		</div>
	</div>
	
	
	
	<!-- Modal Feature Begin -->
	<form id="frmInput" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modalInput" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Form Profile Pengguna</h4>
					
				</div>
				
				<div class="modal-body">
					 <div  id="progressBar"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
					<div class="row">
				      <div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
				      </div>
		    		</div>
						<p class="text-danger">Input dengan tanda (*) wajib diisi.</p>
					
				
					<input type="hidden" id="id" value="0" />
			    	
					
				    
				     <div class="form-group">
				      <div class="col-md-12">
				      	<label>Nama Pengguna *</label>
			    			<select class="form-control" id="user">
			    				<option value="0">-- Pilih Pengguna --</option>
							</select>
				      </div>
				    </div>
				    
					<div class="form-group">
				      <div class="col-md-6">
				      	<label for="birth_date">Tanggal Lahir: </label>
				    	<div class="input-group"> 
					    	<input type="text" class="form-control datepicker" id="birth_date">
					    	<span class="input-group-addon calendar">
					    		<span class="glyphicon glyphicon-calendar"></span>
					    	</span>
				    	</div>
				      </div>
				      
				      <div class="col-md-6">
				      	<label>Jenis Kelamin</label>
				      	<div class="input-group">
					      	<label class="radio-inline">
	        					<input type="radio" name="gender" value="1">Pria</label>
	        				<label class="radio-inline">
					      		<input type="radio" name="gender" value="2">Wanita</label>
					      </div>
				      </div>
				    </div>
				    
				    <div class="form-group">
				      <div class="col-md-12">
				      	<span id="marital_status"></span>
				      </div>
				    </div>
				    
				    <div class="form-group">
				      <div class="col-md-12">
				      	<label>Alamat Rumah</label>
			    			<textarea id="home_address" class="form-control" rows="3"></textarea>
				      </div>
				    </div>
				    
				    <div class="form-group">
				      <div class="col-md-6">
				      	<label>Kota/Kabupaten</label>
			    			<input type="text" class="form-control" id="home_city" placeholder="">
				      </div>
				      
				      <div class="col-md-6">
				      	<label>Propinsi</label>
			    			<select class="form-control" id="home_province">
			    				<option value="0">-- Pilih Propinsi --</option>
							</select>
				      </div>
				    </div>
				    <div class="form-group">
				      <div class="col-md-6">
				      	<label>Kode Pos</label>
			    			<input type="text" class="form-control" id="home_postal_code" placeholder="">
				      </div>
				      
				      
				    </div>
			    	<div class="form-group">
				      <div class="col-md-6">
				      	<label>Handphone</label>
			    			<input type="text" class="form-control" id="mobile_phone" placeholder="">
				      </div>
				      
				      <div class="col-md-6">
				      	<label>Telepon Rumah</label>
			    			<input type="text" class="form-control" id="home_phone_number" placeholder="">
				      </div>
				    </div>
				    
				    <div class="form-group">
					      <div class="col-md-12">
					      	<label>Profil Photo :</label>
				    			<div class="input-group">
				    				<span class="input-group-addon">
							    		<span class="glyphicon glyphicon-file"></span>
							    	</span>
								  <input id="fileName" readonly type="text" class="form-control" aria-label="...">
								  <div class="input-group-btn">
								  	
								    <button id="btnBrowse" type="button" class="btn btn-primary">Pilih</button>
								  </div>
								</div>
								<input class="hidden" type="file" id="uploadFile" data-id="0" onchange="return readUrl()">
					      </div>
					  </div>
				    
			    </div>
			
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
					<button id="btnReset" type="button" class="btn btn-default" aria-hidden="true">RESET</button>
					<button id="btnSave" type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-floppy-disk"></span> 
					SIMPAN</button>
				</div>
			</div>
			</div>
		</div>
	</form>
	
	<!-- Modal Feature-->
	<div class="modal fade" data-backdrop="static" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
		<div class="modal-dialog">
			
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
					<div class="row">
						<div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
						</div>
					</div>
				</div>
				
				<div class="modal-body">		
					<div  id="progressBarRemove"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>		
					<div class="form-group">
						<p>Apakah anda yakin akan menghapus data ini?</p>
					</div>
					
					<div class="modal-footer">
						<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btnRemove" onclick="doRemove($(this).data('id'));" data-id="0" type="button" class="btn btn-primary">
						<span class="glyphicon glyphicon-trash"></span>  
						HAPUS</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	 
	 <script src="js/profile-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>