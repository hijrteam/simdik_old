<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Hijr App</title>

<!-- CSS -->

<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/jquery-ui.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
<link href="css/sticky-footer-navbar.css" rel="stylesheet">
<link href="css/common-style.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

<!-- Favicon and touch icons -->
<link href="img/hijr.ico" rel="shortcut icon" type="image/x-icon" />
