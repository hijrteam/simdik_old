<%@page import="id.co.hijr.app.domain.User"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="id.co.hijr.app.core.Clause"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="id.co.hijr.app.service.AppManagerService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%
Cookie cookie = null;
Cookie[] cookies = null;
// Get an array of Cookies associated with this domain
cookies = request.getCookies();

boolean isLogin = false;
String token = "";

if( cookies != null ){
   for (int i = 0; i < cookies.length; i++){
      cookie = cookies[i];
      System.out.println(cookie.getName());
      if (cookie.getName().equalsIgnoreCase("token")) {
    	  token = cookie.getValue();    	  
      }
     
   }
}

if(!token.equals("")) {
	ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	String today = sdf.format(new Date());
	AppManagerService svc = appCtx.getBean(AppManagerService.class);
	
	List<Clause> filter = new ArrayList<Clause>();
	filter.add(new Clause("token", token));
	filter.add(new Clause("expired",">", today));
	
	System.out.println("header.jsp");
	List<User> lstUser = svc.getUserDao().list(filter);
	if(lstUser.size() > 0) {
		isLogin = true;
	}
}
if (isLogin == false) {
	response.sendRedirect("./login.html");
}
%>