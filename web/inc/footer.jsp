<!-- Modal Feature Begin -->
	<form id="frmChangePassword" class="form-horizontal">
		<div class="modal fade" data-backdrop="static" id="modalChangePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabelPassword" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Change your password</h4>
						
					</div>
					<div class="modal-body">
						<div  id="changePasswordProgress"  class="row">
					      <div class="col-xs-12">
							<div class="progress">
							  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							    <span class="sr-only">45% Complete</span>
							  </div>
							</div>
					      </div>
					    </div>
						<div class="form-group">
							<div class="col-md-12">
								<div id="changePasswordMsg" class="alert alert-success" role="alert" hidden="hidden"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
					      	<label>Current Password</label>
				    			<input type="password" class="form-control" id="changePasswordOld" placeholder="Type your current password">
					      </div>					     
						</div>		
						<div class="form-group">
							<div class="col-md-6">
					      	<label>Kata Kunci</label>
				    			<input type="password" class="form-control" id="changePasswordNew" placeholder="New password">
					      </div>
					      
					      <div class="col-md-6">
					      	<label>Ulangi Kata Kunci</label>
				    			<input type="password" class="form-control" id="changePasswordNewRetype" placeholder="Retype new password">
					      </div>
						</div>
						
						
					</div>
					<div class="modal-footer">
							<button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true">BATAL</button>
						<button id="btnChangePassword" type="submit" class="btn btn-primary">
						<span class="glyphicon glyphicon-floppy-disk"></span> 
						SIMPAN</button>
						</div>
				</div>
			</div>
		</div>
</form>
	<footer class="footer">
      <div class="container">
        <p class="text-muted text-center">Copyright � 2014 - 2016. PT Hijr Global Solution.</p>
      </div>
    </footer>

	<!-- Javascript -->
	<script src="js/libs/jquery-1.11.2.min.js"></script>
	<script src="js/libs/jquery-ui.min.js"></script>	
	<script src="js/libs/bootstrap.min.js"></script>
	<script src="js/libs/bootstrap3-typeahead.min.js"></script>
	<script src="js/libs/jquery.backstretch.min.js"></script>
	<script src="js/libs/jquery.cookie.js"></script>
	<script src="js/libs/jquery-ui.min.js"></script>
	<script src="js/libs/jquery.number.min.js"></script>
	<script src="js/libs/jquery.md5.js"></script>
	<script src="js/libs/bootstrap-datetimepicker.js"></script>
	<script src="js/common-script.js"></script>
	