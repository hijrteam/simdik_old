<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
	
	<script>
	function init(){}
	</script>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	
	<header class="image-bg-fluid-height">
        <img class="img-responsive img-center" src="img/logo.png" width="100" alt="">
    </header>
	
	<div class="container">
		<h2>Selamat datang di aplikasi contoh PT. HIJR GLOBAL SOLUTION.</h2>
		<h4>Didukung oleh: Hijr Software Development Framework v.2</h4>
		
		<p>Bantuan pendukung utama:</p>
		<ul>
			<li>jQuery 1.11 Javascript Framework +  Add-Ons</li>
			<li>Bootstrap 3 CSS Framework + Add-Ons</li>	
			<li>Spring 4 Java Framework</li>	
			<li>MyBatis 3 Persistence Framework</li>		
		</ul>
	</div>
	
	<%@include file="inc/footer.jsp"%>

</body>

</html>