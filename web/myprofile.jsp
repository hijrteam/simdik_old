<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
	<style>
	.datepicker{z-index:1000 !important;}
	</style>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	
	<div class="container">
	
	<form id="frmInput" class="form-horizontal">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title pull-left">Update Your Profile</h3>
		            <div class="btn-toolbar">
						<button id="btnSimpan" type="submit" class="btn btn-primary pull-right">
					        <span class="glyphicon glyphicon-floppy-disk"></span> Simpan
				        </button>
		        
		        </div>
		        <div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<div class="row">
				      <div class="col-md-12">
							<div id="msg" class="alert alert-success" role="alert" hidden="hidden"></div>
				      </div>
		    		</div>
					<input type="hidden" id="id" value="0" />
			    	
				     <div class="form-group">
				      <div class="col-md-12">
				      	<label>Nama Pengguna *</label>
			    			<input type="text" class="form-control" id="user" data-id="0" readonly>
				      </div>
				    </div>
				    
					<div class="form-group">
				      <div class="col-md-6">
				      	<label for="birth_date">Tanggal Lahir: </label>
				    	<div class="input-group"> 
					    	<input type="text" class="form-control datepicker" id="birth_date">
					    	<span class="input-group-addon calendar">
					    		<span class="glyphicon glyphicon-calendar"></span>
					    	</span>
				    	</div>
				      </div>
				      
				      <div class="col-md-6">
				      	<label>Jenis Kelamin</label>
				      	<div class="input-group">
					      	<label class="radio-inline">
	        					<input type="radio" name="gender" value="1">Pria</label>
	        				<label class="radio-inline">
					      		<input type="radio" name="gender" value="2">Wanita</label>
					      </div>
				      </div>
				    </div>
				    
				    <div class="form-group">
				      <div class="col-md-12">
				      	<span id="marital_status"></span>
				      </div>
				    </div>
				    
				    <div class="form-group">
				      <div class="col-md-12">
				      	<label>Alamat Rumah</label>
			    			<textarea id="home_address" class="form-control" rows="3"></textarea>
				      </div>
				    </div>
				    
				    <div class="form-group">
				      <div class="col-md-6">
				      	<label>Kota/Kabupaten</label>
			    			<input type="text" class="form-control" id="home_city" placeholder="">
				      </div>
				      
				      <div class="col-md-6">
				      	<label>Propinsi</label>
			    			<select class="form-control" id="home_province">
			    				<option value="0">-- Pilih Propinsi --</option>
							</select>
				      </div>
				    </div>
				    <div class="form-group">
				      <div class="col-md-6">
				      	<label>Kode Pos</label>
			    			<input type="text" class="form-control" id="home_postal_code" placeholder="">
				      </div>
				      
				      
				    </div>
			    	<div class="form-group">
				      <div class="col-md-6">
				      	<label>Handphone</label>
			    			<input type="text" class="form-control" id="mobile_phone" placeholder="">
				      </div>
				      
				      <div class="col-md-6">
				      	<label>Telepon Rumah</label>
			    			<input type="text" class="form-control" id="home_phone_number" placeholder="">
				      </div>
				    </div>
				    
				    <div class="form-group">
					      <div class="col-md-12">
					      	<label>Profil Photo :</label>
				    			<div class="input-group">
				    				<span class="input-group-addon">
							    		<span class="glyphicon glyphicon-file"></span>
							    	</span>
								  <input id="fileName" readonly type="text" class="form-control" aria-label="...">
								  <div class="input-group-btn">
								  	
								    <button id="btnBrowse" type="button" class="btn btn-primary">Pilih</button>
								  </div>
								</div>
								<input class="hidden" type="file" id="uploadFile" data-id="0" onchange="return readUrl()">
					      </div>
					  </div>
			
						
			
		</div>
	</div>
	</form>
	</div>
	
	<!-- Modal -->
		<div class="modal fade" id="modalProgress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel">Sedang proses...</h4>
		      </div>
		      <div class="modal-body">
		        <div  id="progressBar"  class="row">
				      <div class="col-xs-12">
						<div class="progress">
						  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						    <span class="sr-only">45% Complete</span>
						  </div>
						</div>
				      </div>
				    </div>
		      </div>
		    </div>
		  </div>
		</div>
	 
	
	 
	 <script src="js/myprofile-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>