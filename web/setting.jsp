<%@include file="inc/redirect.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<%@include file="inc/header.jsp"%>
</head>

<body>
	<%@include file="inc/navbar.jsp"%>
	
	<div class="container">
	<form class="form-horizontal" role="form">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title pull-left">
					Setting
		            </h3>
		
		        <button id="btnSave" type="button" class="btn btn-primary pull-right">
				<span class="glyphicon glyphicon-floppy-disk"></span> 
				SIMPAN</button>
		        <div class="clearfix"></div>
			</div>
			<div id="setting" class="panel-body">
				 
				
				
			</div>
		</div>
		</form>
	</div>
	
	
	
	
	 <script src="js/setting-script.js"></script>
	<%@include file="inc/footer.jsp"%>

</body>

</html>