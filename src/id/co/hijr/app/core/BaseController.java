package id.co.hijr.app.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class BaseController {
	
	protected Properties prop = new Properties();

	private Pagination pagination = new Pagination();
	
	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
	
	public BaseController() {
		// TODO Auto-generated constructor stub
		
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
		try {
			prop.load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}

}
