package id.co.hijr.app.core;

public interface ServiceAction {

	public static final String ADD = "add";
	public static final String UPDATE = "update";
	public static final String DELETE = "delete";
	public static final String REMOVE = "remove";
	public static final String LIST = "list";
	public static final String RESTORE = "restore";
	public static final String CLEAN = "clean";
	public static final String EMPTY = "empty";
	public static final String LOGIN = "login";
	public static final String LOGOUT = "logout";
	public static final String GOTO = "goto";
}
