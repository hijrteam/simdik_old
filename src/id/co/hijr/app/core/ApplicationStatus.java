package id.co.hijr.app.core;

public interface ApplicationStatus {

	public static final String SUCCESS = "success";
	public static final String FAILED = "failed";
}
