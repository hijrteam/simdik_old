package id.co.hijr.app.core;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import id.co.hijr.app.dao.RoleDao;
import id.co.hijr.app.dao.SettingDao;
import id.co.hijr.app.dao.UserDao;
import id.co.hijr.app.domain.Setting;
import id.co.hijr.app.domain.User;

public interface BaseService {

	public UserDao getUserDao();
	
	public RoleDao getRoleDao();
	
	public SettingDao getSettingDao();
	
	public User login(String username, String password, Date expired) throws Exception;
	
	public void saveSetting(List<Setting> listSetting) throws Exception;
	
	public List<Setting> getListSetting();
	
	public void logout(String username) throws Exception;
	
	public void changePassword(String username, String oldPassword, String newPassword) throws Exception;
}
