package id.co.hijr.app.core;

public class HijrException extends Exception {
	
	public final static String ERR_DEFAULT = "STDERROR";
	public final static String ERR_DUPLICATE = "DUPLICATE";
	public final static String ERR_UNAUTHORIZED = "UNAUTHORIZED";
	public final static String ERR_UNMATCHPASSWD = "UNMATCHPASSWD";
	
	public HijrException(String errorType, String message){
		super(errorType + ":" +message);
	}

}
