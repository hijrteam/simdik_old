package id.co.hijr.app.core;


public class ResponseWrapper{
	
	public final static String STATUS_OK = "OK";
	public final static String STATUS_ERROR = "ERROR";
	public final static String STATUS_UNAUTHORIZED = "UNAUTHORIZED";
	public final static String STATUS_FAILED = "FAILED";
	
	private String status = "OK";
	private String message;
	private Object data;
	private Pagination pagination = new Pagination();
		
	public Pagination getPagination() {
		return pagination;
	}
	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	

}
