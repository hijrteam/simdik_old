package id.co.hijr.app.core;

public class Clause {

	private String column;
	private String value;
	private String operator;
	
	public Clause() {
		// TODO Auto-generated constructor stub
	}
	
	public Clause(String column, String value) {
		this(column, "=", value);
	}
	
	public Clause(String column, String operator, String value) {
		this.operator = operator;
		this.column = column;
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getValue() {
		if(operator.toLowerCase().contains("is") || operator.toLowerCase().contains("in") || operator.toLowerCase().equals("between")){
			return value;
		}else if(operator.toLowerCase().equals("like")){
			if(value.contains("%")){
				return "'"+value+"'";
			}else{
				return "'%"+value+"%'";
			}
		}
		return "'"+value+"'";
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}
