package id.co.hijr.app.core;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import id.co.hijr.app.domain.User;
import id.co.hijr.app.service.AppManagerService;

public class AccessFilter implements Filter {
	
	final static Logger log = Logger.getLogger(AccessFilter.class);

	private ArrayList<String> urlList;
	
	private FilterConfig config;

	 public void init(FilterConfig config) throws ServletException {
		this.config = config;
        String urls = config.getInitParameter("allow-path");
        StringTokenizer token = new StringTokenizer(urls, ",");
 
        urlList = new ArrayList<String>();
 
        while (token.hasMoreTokens()) {
            urlList.add(token.nextToken());
 
        }
    }
    
    public void destroy() {
    }
 
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
 
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String url = request.getPathInfo();
        String ctx = request.getServletPath();
        
        String token = "";
        
        Cookie cookie = null;
    	Cookie[] cookies = null;
    	// Get an array of Cookies associated with this domain
    	cookies = request.getCookies();
    	
    	
    	if( cookies != null ){
    	   for (int i = 0; i < cookies.length; i++){
    	      cookie = cookies[i];
    	      if (cookie.getName().equalsIgnoreCase("token")) {
    	    	  token = cookie.getValue();
    	    	  //System.out.println("token from cookie: " + token);
    	      }
    	      
    	   }
    	}
    	
    	
    	// default for jsp (/page)
        String redirectUrl = request.getContextPath()+"/rest/public/forbidden";
    	
    	// get token from url parameter if cookie not available
    	if(token.equals("")) token = (request.getParameter("token")==null?"":request.getParameter("token"));
    	
    	
    	User loginUser = null;
    	
    	if(!token.equals("")){
	        ApplicationContext appCtx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String today = sdf.format(new Date());
	    	AppManagerService svc = appCtx.getBean(AppManagerService.class);
	    	
	    	List<Clause> filter = new ArrayList<Clause>();
	    	filter.add(new Clause("token", token));
	    	filter.add(new Clause("expired",">", today));
	    	
	    	List<User> lstUser = svc.getUserDao().list(filter);
	    	
	    	if(lstUser.size() > 0) {
	    		loginUser = lstUser.get(0);
	    	}
	    	
    	}
    	//System.out.println(isLogin+ " token: "+token);
    	
        
        if (!allowedRequest(url)) { // all path require login
            if (loginUser != null) {
            	chain.doFilter(new FilteredRequest(req, loginUser.getUsername(), loginUser.getRole().getName()), res);
            	
        	}else{
        		response.sendRedirect(redirectUrl);
        		return;
        	}
           
        }else{
        	// all path defined in web.xml (e.g: /user)
        	chain.doFilter(req, res);
        }
    }
    
    private boolean allowedRequest(String url){
    	if(url == null) return true;
    	for(String s: urlList){
    		if(url.startsWith(s)) {
    			return true;
    		}
    	}
    	return false;
    }
 
   
    static class FilteredRequest extends HttpServletRequestWrapper {

    	private String username = "";
    	private String rolename = "";

    	public FilteredRequest(ServletRequest request, String username, String rolename) {
    		super((HttpServletRequest)request);
    		this.username = username;
    		this.rolename = rolename;
    	}

    	public String getParameter(String paramName) {
    		String value = super.getParameter(paramName);
    		if ("username".equals(paramName)) {
    			value = this.username;
    		}else if ("rolename".equals(paramName)) {
    			value = this.rolename;
    		}
    		return value;
    	}

    	public String[] getParameterValues(String paramName) {
    		String values[] = super.getParameterValues(paramName);
    		if ("username".equals(paramName)) {
    			return new String[]{this.username};
    		}else if ("rolename".equals(paramName)) {
    			return new String[]{this.rolename};
    		}
    		
    		return values;
    	}
    }

}
