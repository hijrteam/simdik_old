package id.co.hijr.app.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.app.domain.Setting;
import id.co.hijr.app.service.AppManagerService;


@RestController
@RequestMapping("/setting/")
public class SettingRestService extends BaseController {

	@Autowired
	AppManagerService appManagerService  ;
	
	List<Setting> listSetting;
	
	@RequestMapping(value = "/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper list() {
		ResponseWrapper resp = new ResponseWrapper();
		resp.setData(appManagerService.getListSetting());
		return resp;
	}
	
	@RequestMapping(value = "/category", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper category() {
		ResponseWrapper resp = new ResponseWrapper();
		resp.setData(appManagerService.getSettingDao().listSettingCategory());
		return resp;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper save(@RequestBody SettingRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		
		try {
			appManagerService.saveSetting(req.getListSetting());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			resp.setMessage("Only admin who can change setting");
			resp.setStatus(ResponseWrapper.STATUS_UNAUTHORIZED);e.printStackTrace();
		}
		
		return resp;
	}

	public List<Setting> getListSetting() {
		return listSetting;
	}

	public void setListSetting(List<Setting> listSetting) {
		this.listSetting = listSetting;
	}
	
	
	
}
