package id.co.hijr.app.core;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ApplicationUtils {

	public static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	
	public static Date strToDate(String str) {
		Date dt = new Date();
		try {
			dt= sdf.parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dt;
	}
	
	public static String encrypt(String str) {
		String enc = str;
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.update(str.getBytes(),0,str.length());
			enc = new BigInteger(1,m.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return enc;
	}
}
