package id.co.hijr.app.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;

public abstract class DaoBase<T extends Entity > extends QueryBase implements DaoGeneric<T> {
	
	protected Class<T> type;
	
	public DaoBase(SqlSessionFactory sqlSessionFactory, String namespace, Class<T> type) {
		super(sqlSessionFactory, namespace);
		this.type = type;
	}
	
	public DaoBase(SqlSessionFactory sqlSessionFactory, String namespace) {
		this(sqlSessionFactory, namespace,null);
	}
	
	@Override
	public int newId(){
		return getSqlSession().selectOne(getNamespace()+".newid");
	}
	
	@Override
	public int count(){
		return count(new ArrayList<Clause>());
	}
	
	public int count(List<Clause> filter){
		HashMap<String, Object> param = new HashMap<String, Object>();
		if(filter.size() > 0)
			param.put("filter", filter);
		return getSqlSession().selectOne(getNamespace()+".count", param);
	}
	
	@Override
	public int countTrash(){
		return getSqlSession().selectOne(getNamespace()+".countTrash");
	}
	
	@Override
	public void add(T object) throws Exception{
		Entity e = ((Entity) object);
		if(e.getId() < 1) e.setId(newId());
		getSqlSession().insert(getNamespace()+".add", object);
	}
	
	@Override
	public void delete(T object) throws Exception{
		delete(object);
	}
	
	@Override
	public void delete(List<Clause> filter) throws Exception{
		HashMap<String, Object> param = new HashMap<String, Object>();
		if(filter.size() > 0)
			param.put("filter", filter);
		getSqlSession().delete(getNamespace()+".deleteWithFilter", param);
	}
	
	@Override
	public void remove(int id) throws Exception{
		getSqlSession().delete(getNamespace()+".remove", id);
	}

	@Override
	public void remove(List<Clause> filter) throws Exception{
		HashMap<String, Object> param = new HashMap<String, Object>();
		if(filter.size() > 0)
			param.put("filter", filter);
		getSqlSession().delete(getNamespace()+".removeWithFilter", param);
	}
	
	
	@Override
	public void restore(int id) throws Exception{
		getSqlSession().delete(getNamespace()+".restore", id);
	}
	
	@Override
	public void empty() throws Exception{
		getSqlSession().delete(getNamespace()+".empty");
	}
	
	@Override
	public void clean() throws Exception{
		getSqlSession().delete(getNamespace()+".clean");
	}
	
	@Override
	public void update(T object) throws Exception{
		getSqlSession().update(getNamespace()+".update", object);
	}
	
	@Override
	public List<T> list(){
		List<Clause> filter = new ArrayList<Clause>();
		int index = -1;
		int count = -1;
		String order = "";
		return list(filter, index, count, order);	
	}
	
	@Override
	public List<T> list(List<Clause> filter){		
		int index = -1;
		int count = -1;
		String order = "";
		return list(filter, index, count, order);	
	}
	
	@Override
	public List<T> list(List<Clause> filter, String order){
		int index = -1;
		int count = -1;
		return list(filter, index, count, order);	
	}
	
	@Override
	public List<T> list(int index, int count){
		List<Clause> filter = new ArrayList<Clause>();
		String order = "";
		return list(filter, index, count, order);		
	}
	
	@Override
	public List<T> list(int index, int count, String order){
		List<Clause> filter = new ArrayList<Clause>();
		return list(filter, index, count, order);		
	}
	
	@Override
	public List<T> list(String order){
		List<Clause> filter = new ArrayList<Clause>();
		int index = -1;
		int count = -1;
		return list(filter, index, count, order);		
	}
	
	@Override
	public List<T> list(List<Clause> filter, int index, int count){
		String order = "";
		return list(filter, index, count, order);
	}
	
	@Override
	public List<T> list(List<Clause> filter, int index, int count, String order){
		HashMap<String, Object> param = new HashMap<String, Object>();
		String strLimit = "";
		String strOffset = "";
		if(index >= 0 && count >= 0){
			strLimit = "LIMIT " + count;
			strOffset = "OFFSET " + index;
		}
		param.put("limit", strLimit); // number of rows to be display
		param.put("offset", strOffset); // start from row index to be display
		if(!order.equals(""))
			param.put("order", order);
		if(filter.size() > 0)
			param.put("filter", filter);
		if(count == 0) { // if row count parameter set 0, then show all data
			return list("list", this.type, filter,order);
		}
		return list("list", this.type, filter, index, count, order);
	}
	
	@Override
	public T get(int id){
		return getSqlSession().selectOne(getNamespace()+".get", id);
	}
	
	

}
