package id.co.hijr.app.core;

import java.util.ArrayList;
import java.util.List;

public class Pagination {
	
	private List<Clause> filter = new ArrayList<Clause>();
	private int rowcount = 20;
	private int activepage = 1;
	private int recordcount = 1000;
	private String orderby = "";
	
	public int getPagecount() {
		return Double.valueOf(Math.ceil((double)recordcount/rowcount)).intValue();
	}
	public int getRowcount() {
		return rowcount;
	}
	public void setRowcount(int rowcount) {
		this.rowcount = rowcount;
	}
	public int getActivepage() {
		return activepage;
	}
	public void setActivepage(int activepage) {
		this.activepage = activepage;
	}
	public List<Clause> getFilter() {
		return filter;
	}
	public void setFilter(List<Clause> filter) {
		this.filter = filter;
	}
	public int getRecordcount() {
		return recordcount;
	}
	public void setRecordcount(int recordcount) {
		this.recordcount = recordcount;
	}
	public int getIndex() {
		return (getActivepage()-1)*rowcount;
	}
	public String getOrderby() {
		return orderby;
	}
	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
}
