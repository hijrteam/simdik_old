package id.co.hijr.app.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import id.co.hijr.app.dao.RoleDao;
import id.co.hijr.app.dao.SettingDao;
import id.co.hijr.app.dao.UserDao;
import id.co.hijr.app.domain.Setting;
import id.co.hijr.app.domain.User;
import sun.security.action.GetLongAction;

public abstract class BaseServiceImpl  {
	
	@Autowired
	protected UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private SettingDao settingDao;
	
	
	public SettingDao getSettingDao(){
		return settingDao;
	}
	
	public UserDao getUserDao(){
		return userDao;
	}
	
	public RoleDao getRoleDao(){
		return roleDao;
	}
	
	@Transactional(rollbackFor=Exception.class)
	public void saveSetting(List<Setting> listSetting) throws Exception {
		// TODO Auto-generated method stub
		for(Setting o: listSetting){
			settingDao.update(o);
		}
		
	}
	
	public List<Setting> getListSetting(){
		return settingDao.list();
	}
	
	
	@Transactional(rollbackFor=Exception.class)
	public User login(String username, String password, Date expired) throws Exception {
		List<Clause> lc = new ArrayList<Clause>();
		lc.add(new Clause("username", "=", username));
		lc.add(new Clause("password", "=", password));
		System.out.println(username);
		User ret = null;
		List<User> rs = getUserDao().list(lc); 
		if (rs.size() > 0) {
			User o = rs.get(0);			
			// unique code for every login
			String token = ApplicationUtils.encrypt(o.getUsername()+new Date().getTime());
			// set expired from client request at attribute user.expired
			o.setExpired(expired);
			o.setToken(token);
			getUserDao().update(o);
			
			
			
			getUserDao().logging(username,ServiceAction.LOGIN, "", ApplicationStatus.SUCCESS);
			
			return o;
		}else{
			getUserDao().logging(username,ServiceAction.LOGIN, "", ApplicationStatus.FAILED);
		}
		return ret;
	}
	
	@Transactional(rollbackFor=Exception.class)
	public void changePassword(String username, String oldPassword, String newPassword) throws Exception{
		List<Clause> lc = new ArrayList<Clause>();
		lc.add(new Clause("username", "=", username));
		lc.add(new Clause("password", "=", oldPassword));
		User ret = null;
		List<User> rs = getUserDao().list(lc); 
		if (rs.size() > 0) {
			ret = rs.get(0);
			ret.setPassword(newPassword);
			getUserDao().updatePassword(ret);
			getUserDao().logging(username,ServiceAction.UPDATE, "password", ApplicationStatus.SUCCESS);
			
		}else{
			getUserDao().logging(username,ServiceAction.UPDATE, "password", ApplicationStatus.FAILED);
			throw new HijrException(HijrException.ERR_UNMATCHPASSWD, username);
		}
	}
	
	@Transactional(rollbackFor=Exception.class)
	public void logout(String username) throws Exception{
		List<Clause> lc = new ArrayList<Clause>();
		lc.add(new Clause("username", "=", username));
		User ret = null;
		List<User> rs = getUserDao().list(lc); 
		if (rs.size() > 0) {
			ret = rs.get(0);
			ret.setToken("");
			getUserDao().update(ret);
			getUserDao().logging(username,ServiceAction.LOGOUT, "", ApplicationStatus.SUCCESS);
			
		}else{
			getUserDao().logging(username,ServiceAction.LOGOUT, "", ApplicationStatus.FAILED);
			throw new HijrException(HijrException.ERR_UNAUTHORIZED, username);
		}
	}

}
