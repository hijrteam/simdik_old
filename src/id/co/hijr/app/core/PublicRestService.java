package id.co.hijr.app.core;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.app.domain.User;
import id.co.hijr.app.service.AppManagerService;


@RestController
@RequestMapping("/public/")
public class PublicRestService extends BaseController {

	@Autowired
	AppManagerService appManagerService  ;
	
	User user;
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@RequestMapping(value = "/forbidden", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper list() {
		ResponseWrapper resp = new ResponseWrapper();
		resp.setMessage("You cannot access this method");
		resp.setStatus(ResponseWrapper.STATUS_UNAUTHORIZED);
		return resp;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper login(@RequestBody PublicRestService req, HttpServletRequest request, 
	        HttpServletResponse response) throws Exception {
		ResponseWrapper resp = new ResponseWrapper();
		Calendar expired = Calendar.getInstance();
		expired.add(Calendar.DAY_OF_YEAR, 7);
		User loginUser = appManagerService.login(req.getUser().getUsername(), req.getUser().getPassword(), expired.getTime());
		if (loginUser != null) {
			
			resp.setData(loginUser);
		}else {
			resp.setMessage("Login failed, please try again!");
			resp.setStatus(ResponseWrapper.STATUS_FAILED);
		}
		return resp;
	}
	
	
	
}
