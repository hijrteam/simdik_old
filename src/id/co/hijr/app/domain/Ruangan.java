package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Metadata;

public class Ruangan extends Metadata implements Serializable {
	public String namaRuangan;
	//public JenisRuangan jenisRuangan;
	
	public Ruangan() {
		
	}
	public Ruangan(int id) {
		super(id);
	}
	public String getNamaRuangan() {
		return namaRuangan;
	}
	public void setNamaRuangan(String namaRuangan) {
		this.namaRuangan = namaRuangan;
	}
	
}
