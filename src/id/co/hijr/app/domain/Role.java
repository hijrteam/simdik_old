package id.co.hijr.app.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import id.co.hijr.app.core.Metadata;

public class Role extends Metadata implements Serializable {
	
	private String name;
	private String description;
	
	
	public Role() {
			
	}
	public Role(int id) {
		super(id);	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}


