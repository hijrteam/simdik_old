package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Entity;

public class Setting extends Entity implements Serializable {
	
	private String code;
	private String name;
	private String value;
	private SettingCategory category;
	
	public Setting() {
			
	}
	public Setting(int id) {
		super(id);	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public SettingCategory getCategory() {
		return category;
	}
	public void setCategory(SettingCategory category) {
		this.category = category;
	}
	
}


