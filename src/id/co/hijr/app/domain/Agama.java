package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Metadata;

public class Agama extends Metadata implements Serializable {
	public String namaAgama;
	
	public Agama() {
		
	}
	public Agama(int id) {
		super(id);
	}
	public String getNamaAgama() {
		return namaAgama;
	}
	public void setNamaAgama(String namaAgama) {
		this.namaAgama = namaAgama;
	}
}
