package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Metadata;

public class Jenjang extends Metadata implements Serializable {
	public String namaJenjang;
	public String keterangan;
	
	public Jenjang() {
		
	}
	public Jenjang(int id) {
		super(id);
	}
	public String getNamaJenjang() {
		return namaJenjang;
	}
	public void setNamaJenjang(String namaJenjang) {
		this.namaJenjang = namaJenjang;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
}
