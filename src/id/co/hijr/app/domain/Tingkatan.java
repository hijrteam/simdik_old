package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Metadata;

public class Tingkatan extends Metadata implements Serializable {
	public String namaTingkatan;
	public String subTingkatan;
	
	public Tingkatan() {
		
	}
	public Tingkatan(int id) {
		super(id);
	}
	public String getNamaTingkatan() {
		return namaTingkatan;
	}
	public void setNamaTingkatan(String namaTingkatan) {
		this.namaTingkatan = namaTingkatan;
	}
	public String getSubTingkatan() {
		return subTingkatan;
	}
	public void setSubTingkatan(String subTingkatan) {
		this.subTingkatan = subTingkatan;
	}
	
}
