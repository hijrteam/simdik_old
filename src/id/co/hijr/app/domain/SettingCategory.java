package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Metadata;

public class SettingCategory extends Metadata implements Serializable {
	
	private String name;
	
	
	public SettingCategory() {
			
	}
	public SettingCategory(int id) {
		super(id);	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}


