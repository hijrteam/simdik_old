package id.co.hijr.app.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import id.co.hijr.app.core.Metadata;

public class Province extends Metadata implements Serializable {
	
	private String name;
	private String code;
	
	
	public Province() {
			
	}
	public Province(int id) {
		super(id);	
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
		
}


