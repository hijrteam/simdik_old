package id.co.hijr.app.domain;

import java.io.Serializable;
import java.util.Date;

import id.co.hijr.app.core.Metadata;

public class Murid extends Metadata implements Serializable {
	public String namaLengkap;
	public Integer jenisKelamin;
	public String tempatLahir;
	public Date  tanggalLahir;
	public Agama agama;
	public Integer anakKe;
	public Integer jumlahSaudara;
	public String alamat;
	public Tingkatan tingkatan;
	public Ruangan ruangan;
	
	public Murid() {
		
	}
	public Murid(int id) {
		super(id);
	}
	public String getNamaLengkap() {
		return namaLengkap;
	}
	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}
	public Integer getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(Integer jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getTempatLahir() {
		return tempatLahir;
	}
	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public Agama getAgama() {
		return agama;
	}
	public void setAgama(Agama agama) {
		this.agama = agama;
	}
	public Integer getAnakKe() {
		return anakKe;
	}
	public void setAnakKe(Integer anakKe) {
		this.anakKe = anakKe;
	}
	public Integer getJumlahSaudara() {
		return jumlahSaudara;
	}
	public void setJumlahSaudara(Integer jumlahSaudara) {
		this.jumlahSaudara = jumlahSaudara;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public Tingkatan getTingkatan() {
		return tingkatan;
	}
	public void setTingkatan(Tingkatan tingkatan) {
		this.tingkatan = tingkatan;
	}
	public Ruangan getRuangan() {
		return ruangan;
	}
	public void setRuangan(Ruangan ruangan) {
		this.ruangan = ruangan;
	}
	
}
