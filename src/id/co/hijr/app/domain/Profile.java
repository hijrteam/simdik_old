package id.co.hijr.app.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.hijr.app.core.JsonDateSerializer;
import id.co.hijr.app.core.Metadata;

public class Profile extends Metadata implements Serializable {
	
	private User user ;
	private UploadFile picture;
	private String homePhoneNumber;
	private String homeAddress;
	private String homeCity;
	private Province homeProvince;
	private String homePostalCode;
	private String mobilePhone;
	private Date birthDate;
	private Integer gender;
	private MaritalStatus maritalStatus;
	
	public Profile() {
			
	}
	public Profile(int id) {
		super(id);	
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public UploadFile getPicture() {
		return picture;
	}
	public void setPicture(UploadFile picture) {
		this.picture = picture;
	}
	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}
	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}
	public String getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}
	public String getHomeCity() {
		return homeCity;
	}
	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}
	public Province getHomeProvince() {
		return homeProvince;
	}
	public void setHomeProvince(Province homeProvince) {
		this.homeProvince = homeProvince;
	}
	public String getHomePostalCode() {
		return homePostalCode;
	}
	public void setHomePostalCode(String homePostalCode) {
		this.homePostalCode = homePostalCode;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	
}


