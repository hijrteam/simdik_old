package id.co.hijr.app.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import id.co.hijr.app.core.JsonDateTimeDeserializer;
import id.co.hijr.app.core.JsonDateTimeSerializer;
import id.co.hijr.app.core.Metadata;

public class User extends Metadata implements Serializable {

	private String name;
	private String username;
	private String password;
	private String email;
	private String currentPassword;
	private String description;
	private String token;
	@JsonDeserialize(using=JsonDateTimeDeserializer.class)
	private Date expired;
	private Role role;
	
	public User() {
		super();
	}
	
	public User(int id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@JsonSerialize(using=JsonDateTimeSerializer.class)
	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	
}

