package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Metadata;

public class JenisRuangan extends Metadata implements Serializable  {
	public String namaJenisRuangan;
	
	public JenisRuangan() {
		
	}
	public JenisRuangan(int id) {
		super(id);
	}
	public String getNamaJenisRuangan() {
		return namaJenisRuangan;
	}
	public void setNamaJenisRuangan(String namaJenisRuangan) {
		this.namaJenisRuangan = namaJenisRuangan;
	}
	
}
