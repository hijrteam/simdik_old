package id.co.hijr.app.domain;

import java.io.Serializable;

import id.co.hijr.app.core.Metadata;

public class Institusi extends Metadata implements Serializable {
	public String namaInstitusi;
	public String alamatInstitusi;
	public String kodePos;
	public Integer nomorTelepon;
	public Integer nomorFaks;
	public String email;
	public Jenjang jenjang;
	public Integer statusInstitusi;
	public String keterangan;
	
	public Institusi() {
		
	}
	public Institusi(int id) {
		super(id);
	}
	public String getNamaInstitusi() {
		return namaInstitusi;
	}
	public void setNamaInstitusi(String namaInstitusi) {
		this.namaInstitusi = namaInstitusi;
	}
	public String getAlamatInstitusi() {
		return alamatInstitusi;
	}
	public void setAlamatInstitusi(String alamatInstitusi) {
		this.alamatInstitusi = alamatInstitusi;
	}
	public String getKodePos() {
		return kodePos;
	}
	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}
	public Integer getNomorTelepon() {
		return nomorTelepon;
	}
	public void setNomorTelepon(Integer nomorTelepon) {
		this.nomorTelepon = nomorTelepon;
	}
	public Integer getNomorFaks() {
		return nomorFaks;
	}
	public void setNomorFaks(Integer nomorFaks) {
		this.nomorFaks = nomorFaks;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Jenjang getJenjang() {
		return jenjang;
	}
	public void setJenjang(Jenjang jenjang) {
		this.jenjang = jenjang;
	}
	public Integer getStatusInstitusi() {
		return statusInstitusi;
	}
	public void setStatusInstitusi(Integer statusInstitusi) {
		this.statusInstitusi = statusInstitusi;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
}
