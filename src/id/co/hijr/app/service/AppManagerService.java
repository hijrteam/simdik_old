package id.co.hijr.app.service;

import java.util.List;

import id.co.hijr.app.core.BaseService;
import id.co.hijr.app.core.Clause;
import id.co.hijr.app.dao.AgamaDao;
import id.co.hijr.app.dao.InstitusiDao;
import id.co.hijr.app.dao.JenisRuanganDao;
import id.co.hijr.app.dao.JenjangDao;
import id.co.hijr.app.dao.MaritalStatusDao;
import id.co.hijr.app.dao.MuridDao;
import id.co.hijr.app.dao.ProfileDao;
import id.co.hijr.app.dao.ProvinceDao;
import id.co.hijr.app.dao.RuanganDao;
import id.co.hijr.app.dao.TingkatanDao;
import id.co.hijr.app.dao.UploadFileDao;
import id.co.hijr.app.domain.Agama;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.JenisRuangan;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Murid;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.domain.Ruangan;
import id.co.hijr.app.domain.Tingkatan;
import id.co.hijr.app.domain.UploadFile;
import id.co.hijr.app.domain.User;

public interface AppManagerService extends BaseService {

	public MaritalStatusDao getMaritalStatusDao();
	public ProvinceDao getProvinceDao();
	
	// Begin UploadFile methods
	public UploadFileDao getUploadFileDao();
	public UploadFile saveUploadFile(UploadFile uploadFile, String username) throws Exception;
	public void removeUploadFile(List<Clause> filter, String username) throws Exception;
	// End UploadFile methods
	
	// Begin Profile methods
	public ProfileDao getProfileDao();
	public void saveProfile(List<Profile> listProfile, String username) throws Exception;
	public void removeProfile(List<Clause> filter, String username) throws Exception;
	// End Profile methods

	// Begin user methods
	public void saveUser(List<User> listUser, String username) throws Exception;
	public void removeUser(List<Clause> filter, String username) throws Exception;
	// End user methods

	public JenjangDao getJenjangDao();
	public void saveJenjang(List<Jenjang> listJenjang, String username) throws Exception;
	public void removeJenjang(List<Clause> filter, String username) throws Exception;

	public InstitusiDao getInstitusiDao();
	public void saveInstitusi(List<Institusi> listJenjang, String username) throws Exception;
	public void removeInstitusi(List<Clause> filter, String username) throws Exception;

	public MuridDao getMuridDao();
	public void saveMurid(List<Murid> listMurid, String username) throws Exception;
	public void removeMurid(List<Clause> filter, String username) throws Exception;

	public AgamaDao getAgamaDao();
	public void saveAgama(List<Agama> listMurid, String username) throws Exception;
	public void removeAgama(List<Clause> filter, String username) throws Exception;

	public TingkatanDao getTingkatanDao();
	public void saveTingkatan(List<Tingkatan> listMurid, String username) throws Exception;
	public void removeTingkatan(List<Clause> filter, String username) throws Exception;

	public RuanganDao getRuanganDao();
	public void saveRuangan(List<Ruangan> listMurid, String username) throws Exception;
	public void removeRuangan(List<Clause> filter, String username) throws Exception;

	public JenisRuanganDao getJenisRuanganDao();
	public void saveJenisRuangan(List<JenisRuangan> listMurid, String username) throws Exception;
	public void removeJenisRuangan(List<Clause> filter, String username) throws Exception;
}
