package id.co.hijr.app.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import id.co.hijr.app.core.BaseServiceImpl;
import id.co.hijr.app.core.Clause;
import id.co.hijr.app.core.HijrException;
import id.co.hijr.app.core.ServiceAction;
import id.co.hijr.app.dao.AgamaDao;
import id.co.hijr.app.dao.InstitusiDao;
import id.co.hijr.app.dao.JenisRuanganDao;
import id.co.hijr.app.dao.JenjangDao;
import id.co.hijr.app.dao.MaritalStatusDao;
import id.co.hijr.app.dao.MuridDao;
import id.co.hijr.app.dao.ProfileDao;
import id.co.hijr.app.dao.ProvinceDao;
import id.co.hijr.app.dao.RuanganDao;
import id.co.hijr.app.dao.TingkatanDao;
import id.co.hijr.app.dao.UploadFileDao;
import id.co.hijr.app.domain.Agama;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.JenisRuangan;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Murid;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.domain.Ruangan;
import id.co.hijr.app.domain.Tingkatan;
import id.co.hijr.app.domain.UploadFile;
import id.co.hijr.app.domain.User;

@Named
public class AppManagerServiceImpl extends BaseServiceImpl implements AppManagerService, Serializable {
	
	@Autowired
	protected UploadFileDao uploadFileDao;
	
	@Autowired
	protected ProfileDao profileDao;
	
	@Autowired
	protected ProvinceDao provinceDao;
	
	@Autowired
	protected MaritalStatusDao maritalStatusDao;
	
	@Autowired
	protected JenjangDao jenjangDao;

	@Autowired
	protected InstitusiDao institusiDao;

	@Autowired
	protected MuridDao muridDao;

	@Autowired
	protected AgamaDao agamaDao;
	
	@Autowired
	protected TingkatanDao tingkatanDao;
	
	@Autowired
	protected RuanganDao ruanganDao;
	
	@Autowired
	protected JenisRuanganDao jenisRuanganDao;
	
	public MaritalStatusDao getMaritalStatusDao(){
		return maritalStatusDao;	
	}
	
	public ProvinceDao getProvinceDao(){
		return provinceDao;
	}
		
	public JenjangDao getJenjangDao() {
		return jenjangDao;
	}

	public void setJenjangDao(JenjangDao jenjangDao) {
		this.jenjangDao = jenjangDao;
	}
	
	public InstitusiDao getInstitusiDao() {
		return institusiDao;
	}

	public void setInstitusiDao(InstitusiDao institusiDao) {
		this.institusiDao = institusiDao;
	}
	
	public MuridDao getMuridDao() {
		return muridDao;
	}

	public void setMuridDao(MuridDao muridDao) {
		this.muridDao = muridDao;
	}
	
	public AgamaDao getAgamaDao() {
		return agamaDao;
	}

	public void setAgamaDao(AgamaDao agamaDao) {
		this.agamaDao = agamaDao;
	}
	
	

	public TingkatanDao getTingkatanDao() {
		return tingkatanDao;
	}

	public void setTingkatanDao(TingkatanDao tingkatanDao) {
		this.tingkatanDao = tingkatanDao;
	}

	public RuanganDao getRuanganDao() {
		return ruanganDao;
	}

	public void setRuanganDao(RuanganDao ruanganDao) {
		this.ruanganDao = ruanganDao;
	}
	
	public JenisRuanganDao getJenisRuanganDao() {
		return jenisRuanganDao;
	}

	public void setJenisRuanganDao(JenisRuanganDao jenisRuanganDao) {
		this.jenisRuanganDao = jenisRuanganDao;
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeUser(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<User> l = getUserDao().list(filter);
				for(User o: l){
					o.setUserDeleted(username);
					getUserDao().delete(o);
				}
			}else{
				getUserDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
	
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveUser(List<User> listUser, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(User o: listUser){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("username", o.getUsername()));
				
				if(userDao.list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getUsername());
				}
				o.setId(userDao.newId());
				o.setUserAdded(username);
				userDao.add(o);	
				
			}else{
				o.setUserModified(username);
				if(o.getPassword().equals("")){
					userDao.update(o);					
				}else{
					userDao.updatePassword(o);
				}
				
			}
			
		}
	}
	
	
	@Override
	public UploadFileDao getUploadFileDao() {
		// TODO Auto-generated method stub
		return uploadFileDao;
	}

	@Override
	public UploadFile saveUploadFile(UploadFile uploadFile, String username) throws Exception {
		// TODO Auto-generated method stub
		
		uploadFile.setId(uploadFileDao.newId());
		uploadFile.setUserAdded(username);
		uploadFileDao.add(uploadFile);
		
		return uploadFile;
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeUploadFile(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<UploadFile> l = getUploadFileDao().list(filter);
				for(UploadFile o: l){
					o.setUserDeleted(username);
					getUploadFileDao().delete(o);
				}
			}else{
				getUploadFileDao().remove(filter);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeProfile(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<Profile> l = getProfileDao().list(filter);
				for(Profile o: l){
					o.setUserDeleted(username);
					getProfileDao().delete(o);
				}
			}else{
				getProfileDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
	
	public ProfileDao getProfileDao(){
		return profileDao;
	}
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveProfile(List<Profile> listProfile, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(Profile o: listProfile){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("username", o.getUser().getUsername()));
				
				if(getProfileDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getUser().getUsername());
				}
				o.setId(getProfileDao().newId());
				o.setUserAdded(username);
				getProfileDao().add(o);	
				
			}else{
				o.setUserModified(username);
				getProfileDao().update(o);					
			}
			
		}
	}
	
	//new

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveJenjang(List<Jenjang> listJenjang, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(Jenjang o: listJenjang){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("nama_jenjang", o.getNamaJenjang()));
				
				if(getJenjangDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getNamaJenjang());
				}
				o.setId(getJenjangDao().newId());
				o.setUserAdded(username);
				getJenjangDao().add(o);
				
			}else{
				o.setUserModified(username);
				getJenjangDao().update(o);
				
			}
			
		}
	}
	

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeJenjang(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<Jenjang> l = getJenjangDao().list(filter);
				for(Jenjang o: l){
					o.setUserDeleted(username);
					getJenjangDao().delete(o);
				}
			}else{
				getJenjangDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveInstitusi(List<Institusi> listInstitusi, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(Institusi o: listInstitusi){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("nama_institusi", o.getNamaInstitusi()));
				
				if(getInstitusiDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getNamaInstitusi());
				}
				o.setId(getInstitusiDao().newId());
				o.setUserAdded(username);
				getInstitusiDao().add(o);
				
			}else{
				o.setUserModified(username);
				getInstitusiDao().update(o);
				
			}
			
		}
	}	

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeInstitusi(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<Institusi> l = getInstitusiDao().list(filter);
				for(Institusi o: l){
					o.setUserDeleted(username);
					getInstitusiDao().delete(o);
				}
			}else{
				getInstitusiDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveMurid(List<Murid> listMurid, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(Murid o: listMurid){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("nama_lengkap", o.getNamaLengkap()));
				
				if(getMuridDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getNamaLengkap());
				}
				o.setId(getMuridDao().newId());
				o.setUserAdded(username);
				getMuridDao().add(o);
				
			}else{
				o.setUserModified(username);
				getMuridDao().update(o);
				
			}
			
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeMurid(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<Murid> l = getMuridDao().list(filter);
				for(Murid o: l){
					o.setUserDeleted(username);
					getMuridDao().delete(o);
				}
			}else{
				getMuridDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveAgama(List<Agama> listAgama, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(Agama o: listAgama){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("nama_agama", o.getNamaAgama()));
				
				if(getMuridDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getNamaAgama());
				}
				o.setId(getAgamaDao().newId());
				o.setUserAdded(username);
				getAgamaDao().add(o);
				
			}else{
				o.setUserModified(username);
				getAgamaDao().update(o);				
			}
			
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeAgama(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<Agama> l = getAgamaDao().list(filter);
				for(Agama o: l){
					o.setUserDeleted(username);
					getAgamaDao().delete(o);
				}
			}else{
				getAgamaDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveTingkatan(List<Tingkatan> listTingkatan, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(Tingkatan o: listTingkatan){
			// add user when have no id number
			if(o.getId() < 1) {
				//filter.clear();
				//filter.add(new Clause("nama_tingkatan", o.getNamaTingkatan()));
				
				/*if(getTingkatanDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getNamaTingkatan());
				}*/
				o.setId(getTingkatanDao().newId());
				o.setUserAdded(username);
				getTingkatanDao().add(o);
				
			}else{
				o.setUserModified(username);
				getTingkatanDao().update(o);				
			}
			
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeTingkatan(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<Tingkatan> l = getTingkatanDao().list(filter);
				for(Tingkatan o: l){
					o.setUserDeleted(username);
					getTingkatanDao().delete(o);
				}
			}else{
				getTingkatanDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveRuangan(List<Ruangan> listRuangan, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(Ruangan o: listRuangan){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("nama_ruangan", o.getNamaRuangan()));
				
				if(getRuanganDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getNamaRuangan());
				}
				o.setId(getRuanganDao().newId());
				o.setUserAdded(username);
				getRuanganDao().add(o);
				
			}else{
				o.setUserModified(username);
				getRuanganDao().update(o);				
			}
			
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeRuangan(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<Ruangan> l = getRuanganDao().list(filter);
				for(Ruangan o: l){
					o.setUserDeleted(username);
					getRuanganDao().delete(o);
				}
			}else{
				getRuanganDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveJenisRuangan(List<JenisRuangan> listJenisRuangan, String username) throws Exception {
		// TODO Auto-generated method stub
		List<Clause> filter = new ArrayList<>();
		for(JenisRuangan o: listJenisRuangan){
			// add user when have no id number
			if(o.getId() < 1) {
				filter.clear();
				filter.add(new Clause("nama_jenis_ruangan", o.getNamaJenisRuangan()));
				
				if(getJenisRuanganDao().list(filter).size() > 0){
					throw new HijrException(HijrException.ERR_DUPLICATE, o.getNamaJenisRuangan());
				}
				o.setId(getJenisRuanganDao().newId());
				o.setUserAdded(username);
				getJenisRuanganDao().add(o);
				
			}else{
				o.setUserModified(username);
				getJenisRuanganDao().update(o);				
			}
			
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void removeJenisRuangan(List<Clause> filter, String username) throws Exception {
			
		try {
			if(!username.equals("")){
				List<JenisRuangan> l = getJenisRuanganDao().list(filter);
				for(JenisRuangan o: l){
					o.setUserDeleted(username);
					getJenisRuanganDao().delete(o);
				}
			}else{
				getJenisRuanganDao().remove(filter);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new HijrException(HijrException.ERR_DEFAULT, ServiceAction.REMOVE);
			//e.printStackTrace();
		}
	}
}

