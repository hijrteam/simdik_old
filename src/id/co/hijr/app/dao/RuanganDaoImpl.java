package id.co.hijr.app.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.Agama;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.domain.Ruangan;
import id.co.hijr.app.domain.Tingkatan;

@Named
public class RuanganDaoImpl  extends DaoBase<Ruangan> implements RuanganDao, Serializable {

	@Inject
	public RuanganDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, RuanganDao.class.getName(), Ruangan.class);
		// TODO Auto-generated constructor stub
	}

	

}
