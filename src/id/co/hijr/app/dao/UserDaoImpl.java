package id.co.hijr.app.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.User;

@Named
public class UserDaoImpl  extends DaoBase<User> implements UserDao, Serializable {

	@Inject
	public UserDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, UserDao.class.getName(), User.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Log user for each success login
	 */
	@Override
	public void logging(String username, String action, String  resources, String  status) {
		HashMap<String, Object> param = new HashMap<>();
		param.put("username", username);
		param.put("status", status);
		param.put("action", action);
		param.put("resources", resources);
		insert("logging",param);
		
	}

	/**
	 * check role access to end point (JSP Page/Restful)
	 */
	@Override
	public int checkPoint(String roleName, String endPoint){
		// TODO Auto-generated method stub
		HashMap<String, String> param = new HashMap<>();
		param.put("role_name", roleName);
		param.put("end_point", endPoint);
		return getSqlSession().selectOne(getNamespace()+".checkPoint",param);
	}
	
	@Override
	public void updatePassword(User user) throws Exception{
		update("updatePassword", user);
	}
	
}
