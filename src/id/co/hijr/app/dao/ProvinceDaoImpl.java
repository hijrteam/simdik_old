package id.co.hijr.app.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.Province;

@Named
public class ProvinceDaoImpl  extends DaoBase<Province> implements ProvinceDao, Serializable {

	@Inject
	public ProvinceDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, ProvinceDao.class.getName(), Province.class);
		// TODO Auto-generated constructor stub
	}

	

}
