package id.co.hijr.app.dao;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.Province;

public interface ProvinceDao extends DaoGeneric<Province> {

}
