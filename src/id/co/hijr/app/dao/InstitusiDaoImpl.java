package id.co.hijr.app.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Profile;

@Named
public class InstitusiDaoImpl  extends DaoBase<Institusi> implements InstitusiDao, Serializable {

	@Inject
	public InstitusiDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, InstitusiDao.class.getName(), Institusi.class);
		// TODO Auto-generated constructor stub
	}

	

}
