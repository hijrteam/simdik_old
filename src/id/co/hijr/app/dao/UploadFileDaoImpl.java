package id.co.hijr.app.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.UploadFile;

@Named
public class UploadFileDaoImpl  extends DaoBase<UploadFile> implements UploadFileDao, Serializable {

	@Inject
	public UploadFileDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, UploadFileDao.class.getName(), UploadFile.class);
		// TODO Auto-generated constructor stub
	}

	

}
