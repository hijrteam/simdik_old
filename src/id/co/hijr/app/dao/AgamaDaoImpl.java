package id.co.hijr.app.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.Agama;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.domain.Tingkatan;

@Named
public class AgamaDaoImpl  extends DaoBase<Agama> implements AgamaDao, Serializable {

	@Inject
	public AgamaDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, AgamaDao.class.getName(), Agama.class);
		// TODO Auto-generated constructor stub
	}

	

}
