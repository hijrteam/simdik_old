package id.co.hijr.app.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.Agama;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.JenisRuangan;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.domain.Tingkatan;

@Named
public class JenisRuanganDaoImpl  extends DaoBase<JenisRuangan> implements JenisRuanganDao, Serializable {

	@Inject
	public JenisRuanganDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, JenisRuanganDao.class.getName(), JenisRuangan.class);
		// TODO Auto-generated constructor stub
	}

	

}
