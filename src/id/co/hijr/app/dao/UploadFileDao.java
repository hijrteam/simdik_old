package id.co.hijr.app.dao;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.UploadFile;

public interface UploadFileDao extends DaoGeneric<UploadFile> {

}
