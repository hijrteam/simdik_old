package id.co.hijr.app.dao;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.Profile;

public interface ProfileDao extends DaoGeneric<Profile> {

}
