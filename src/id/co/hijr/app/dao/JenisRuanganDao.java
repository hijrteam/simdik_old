package id.co.hijr.app.dao;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.Agama;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.JenisRuangan;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.domain.Tingkatan;

public interface JenisRuanganDao extends DaoGeneric<JenisRuangan> {

}
