package id.co.hijr.app.dao;

import java.util.List;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.Setting;
import id.co.hijr.app.domain.SettingCategory;

public interface SettingDao extends DaoGeneric<Setting> {

	public List<SettingCategory> listSettingCategory();
}
