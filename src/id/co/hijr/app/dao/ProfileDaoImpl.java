package id.co.hijr.app.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.Profile;

@Named
public class ProfileDaoImpl  extends DaoBase<Profile> implements ProfileDao, Serializable {

	@Inject
	public ProfileDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, ProfileDao.class.getName(), Profile.class);
		// TODO Auto-generated constructor stub
	}

	

}
