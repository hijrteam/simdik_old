package id.co.hijr.app.dao;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.MaritalStatus;

public interface MaritalStatusDao extends DaoGeneric<MaritalStatus> {

}
