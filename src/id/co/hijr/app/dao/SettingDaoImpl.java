package id.co.hijr.app.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import id.co.hijr.app.core.DaoBase;
import id.co.hijr.app.domain.Setting;
import id.co.hijr.app.domain.SettingCategory;

@Named
public class SettingDaoImpl  extends DaoBase<Setting> implements SettingDao, Serializable {

	@Inject
	public SettingDaoImpl(@Qualifier("sqlSessionFactory")SqlSessionFactory sqlSessionFactory) {
		super(sqlSessionFactory, SettingDao.class.getName(), Setting.class);
		// TODO Auto-generated constructor stub
	}

	public List<SettingCategory> listSettingCategory(){
		return list("listSettingCategory", SettingCategory.class);
	}

}
