package id.co.hijr.app.dao;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Profile;

public interface InstitusiDao extends DaoGeneric<Institusi> {

}
