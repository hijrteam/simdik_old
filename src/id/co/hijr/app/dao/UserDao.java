package id.co.hijr.app.dao;

import id.co.hijr.app.core.DaoGeneric;
import id.co.hijr.app.domain.User;


public interface UserDao extends DaoGeneric<User> {
	
	public void logging(String username, String action, String  resources, String  status);
	
	public int checkPoint(String roleName, String endPoint);
	
	public void updatePassword(User user) throws Exception;


}
