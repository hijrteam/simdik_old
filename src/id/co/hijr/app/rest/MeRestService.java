package id.co.hijr.app.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.app.core.BaseController;
import id.co.hijr.app.core.Clause;
import id.co.hijr.app.core.ResponseWrapper;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.service.AppManagerService;

@Scope("session")
@RestController
@RequestMapping("/me/")
public class MeRestService extends BaseController implements Serializable {

	@Autowired
	AppManagerService appManagerService  ;
	
	private Profile profile;
	
	public MeRestService() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper profile(@RequestParam String username) {	
		ResponseWrapper resp = new ResponseWrapper();
		
		ArrayList<Clause> filter = new ArrayList<>();
		filter.add(new Clause("username",username));
		
		Profile o = appManagerService.getProfileDao().list(filter).get(0);
		
		resp.setData(o);
		return resp;
	}
	
	@RequestMapping(value = "/saveProfile", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper save(@RequestParam String username, @RequestBody MeRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			List<Clause> filter = new ArrayList<>();
			filter.add(new Clause("username",username));
			Profile meProfile = appManagerService.getProfileDao().list(filter).get(0);
			
			meProfile.setBirthDate(req.getProfile().getBirthDate());
			meProfile.setGender(req.getProfile().getGender());
			meProfile.setMaritalStatus(req.getProfile().getMaritalStatus());
			meProfile.setHomeAddress(req.getProfile().getHomeAddress());
			meProfile.setHomeCity(req.getProfile().getHomeCity());
			meProfile.setHomePostalCode(req.getProfile().getHomePostalCode());
			meProfile.setHomePhoneNumber(req.getProfile().getHomePhoneNumber());
			meProfile.setHomeProvince(req.getProfile().getHomeProvince());
			meProfile.setMobilePhone(req.getProfile().getMobilePhone());
			meProfile.setPicture(req.getProfile().getPicture());
			
			List<Profile> lstProfile = new ArrayList<>();
			lstProfile.add(meProfile);
			appManagerService.saveProfile(lstProfile,username);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}
	

	@RequestMapping(value = "/logout", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper logout(@RequestParam String username) {	
		ResponseWrapper resp = new ResponseWrapper();
		
		try {
			appManagerService.logout(username);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		
		return resp;
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper changePassword(@RequestParam String username, @RequestBody UserRestService req) {	
		ResponseWrapper resp = new ResponseWrapper();
		
		try {
			appManagerService.changePassword(username, req.getUser().getCurrentPassword(), req.getUser().getPassword());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		
		resp.setData(req.getUser());
		return resp;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}


}
