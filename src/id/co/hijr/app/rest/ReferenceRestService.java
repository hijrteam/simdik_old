package id.co.hijr.app.rest;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.app.core.BaseController;
import id.co.hijr.app.core.ResponseWrapper;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.MaritalStatus;
import id.co.hijr.app.domain.Province;
import id.co.hijr.app.domain.Role;
import id.co.hijr.app.domain.User;
import id.co.hijr.app.service.AppManagerService;

@Scope("session")
@RestController
@RequestMapping("/list/")
public class ReferenceRestService extends BaseController implements Serializable {

	@Autowired
	AppManagerService appManagerService  ;
	
	public ReferenceRestService() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@RequestMapping(value = "/role/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper role() {	
		ResponseWrapper resp = new ResponseWrapper();
		
		List<Role> data = appManagerService.getRoleDao().list();
		resp.setData(data);
		
		return resp;
	}
	
	@RequestMapping(value = "/maritalstatus/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper maritalStatus() {	
		ResponseWrapper resp = new ResponseWrapper();
		
		List<MaritalStatus> data = appManagerService.getMaritalStatusDao().list();
		resp.setData(data);
		
		return resp;
	}
	
	@RequestMapping(value = "/province/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper province() {	
		ResponseWrapper resp = new ResponseWrapper();
		
		List<Province> data = appManagerService.getProvinceDao().list();
		resp.setData(data);
		
		return resp;
	}
	
	@RequestMapping(value = "/jenjang/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper jenjang() {	
		ResponseWrapper resp = new ResponseWrapper();
		
		List<Jenjang> data = appManagerService.getJenjangDao().list();
		resp.setData(data);
		
		return resp;
	}
	
	@RequestMapping(value = "/user/", method = RequestMethod.GET,headers="Accept=application/json")
	public ResponseWrapper user(@RequestParam String rolename) {	
		ResponseWrapper resp = new ResponseWrapper();
		if(rolename.equals("admin")){
			List<User> data = appManagerService.getUserDao().list();
			resp.setData(data);
		}else{
			resp.setMessage("Not authorized.");
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		
		
		
		return resp;
	}
	

}
