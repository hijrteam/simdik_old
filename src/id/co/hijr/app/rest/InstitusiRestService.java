package id.co.hijr.app.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.app.core.BaseController;
import id.co.hijr.app.core.Pagination;
import id.co.hijr.app.core.ResponseWrapper;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.MaritalStatus;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.service.AppManagerService;

@Scope("session")
@RestController
@RequestMapping("/institusi/")
public class InstitusiRestService extends BaseController implements Serializable {

	@Autowired
	AppManagerService appManagerService  ;
	
	Institusi institusi;
	List<Institusi> listInstitusi= new ArrayList<Institusi>();
	
	public InstitusiRestService() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper list(@RequestParam String username, @RequestBody InstitusiRestService req) {	
		ResponseWrapper resp = new ResponseWrapper();
		// get pagination properties from request if profile change their pagination
		Pagination p = req.getPagination();
		p.setRecordcount(appManagerService.getInstitusiDao().count(p.getFilter()));
		
		List<Institusi> data = appManagerService.getInstitusiDao().list(p.getFilter(), p.getIndex(), p.getRowcount());
		resp.setData(data);
		resp.setPagination(p);
		return resp;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper delete(@RequestParam String username, @RequestBody InstitusiRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			appManagerService.removeInstitusi(req.getPagination().getFilter(),"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper save(@RequestParam String username, @RequestBody InstitusiRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			
			if(req.getListInstitusi().size() == 0)
				req.getListInstitusi().add(req.getInstitusi());
			appManagerService.saveInstitusi(req.getListInstitusi(),"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}

	public Institusi getInstitusi() {
		return institusi;
	}

	public void setInstitusi(Institusi institusi) {
		this.institusi = institusi;
	}

	public List<Institusi> getListInstitusi() {
		return listInstitusi;
	}

	public void setListInstitusi(List<Institusi> listInstitusi) {
		this.listInstitusi = listInstitusi;
	}

	
}
