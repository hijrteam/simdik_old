package id.co.hijr.app.rest;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import id.co.hijr.app.core.BaseController;
import id.co.hijr.app.core.Pagination;
import id.co.hijr.app.core.ResponseWrapper;
import id.co.hijr.app.domain.Role;
import id.co.hijr.app.domain.UploadFile;
import id.co.hijr.app.service.AppManagerService;

@Scope("session")
@RestController
@RequestMapping("/uploadFile/")
public class UploadFileRestService extends BaseController implements Serializable {

	@Autowired
	AppManagerService appManagerService;

	UploadFile uploadFile;
	List<UploadFile> listUploadFile = new ArrayList<UploadFile>();
	
	public UploadFileRestService() {
		// TODO Auto-generated constructor stub
		super();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseWrapper get(@RequestParam String username, @PathVariable int id) {
		ResponseWrapper resp = new ResponseWrapper();
		UploadFile data = appManagerService.getUploadFileDao().get(id);
		resp.setData(data);


		return resp;
	}
	
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public void propinsi(@RequestParam String username, @PathVariable int id, HttpServletRequest request, HttpServletResponse response)  throws Exception {
		
		UploadFile data = appManagerService.getUploadFileDao().get(id);
		
		
		String webpath = request.getServletContext().getRealPath("/");
		
		String folder = prop.getProperty("upload.folder");
		if(folder != null){
			webpath = folder;
		}
		
		String filepath = webpath + "/files/" + data.getFolder() + "/" + data.getName();

		
		response.setContentType(data.getType());
		response.setHeader("Content-Disposition", "attachment; filename=\""+data.getName().replace("\"", "")+"\"");
		
		FileInputStream fileInputStream = new FileInputStream(filepath);
		OutputStream responseOutputStream = response.getOutputStream();
		int bytes;
		while ((bytes = fileInputStream.read()) != -1) {
			responseOutputStream.write(bytes);
		}
		fileInputStream.close();
	}

	@RequestMapping(value = "/", method = RequestMethod.POST, headers = { "Accept=application/json",
			"Content-type=application/json" })
	public ResponseWrapper list(@RequestParam String username, @RequestBody UploadFileRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		// get pagination properties from request if user change their
		// pagination
		Pagination p = req.getPagination();
		p.setRecordcount(appManagerService.getUploadFileDao().count(p.getFilter()));

		List<UploadFile> data = appManagerService.getUploadFileDao().list(p.getFilter(), p.getIndex(), p.getRowcount());
		resp.setData(data);
		resp.setPagination(p);
		return resp;
	}


	@RequestMapping(value = "/delete", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper delete(@RequestParam String username, @RequestBody UserRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			appManagerService.removeUploadFile(req.getPagination().getFilter(),"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseWrapper upload(@RequestParam String username, MultipartHttpServletRequest request, HttpServletResponse response) {
		ResponseWrapper resp = new ResponseWrapper();
		// 1. build an iterator
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = null;

		String webpath = request.getServletContext().getRealPath("/");
		
		String folder = prop.getProperty("upload.folder");
		if(folder != null){
			webpath = folder;
		}
		
		
		mpf = request.getFile(itr.next());
		System.out.println(mpf.getOriginalFilename() + " uploaded! " + request.getParameter("folder"));

		uploadFile = new UploadFile();
		uploadFile.setName(mpf.getOriginalFilename());
		uploadFile.setSize(mpf.getSize() / 1024);
		uploadFile.setType(mpf.getContentType());
		uploadFile.setFolder(request.getParameter("folder"));

		
		
		try {

			FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(webpath + "/files/" + uploadFile.getFolder() + "/"  +mpf.getOriginalFilename()));
			
			resp.setData(appManagerService.saveUploadFile(uploadFile,""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}

		return resp;
	}

	public UploadFile getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(UploadFile uploadFile) {
		this.uploadFile = uploadFile;
	}

	public List<UploadFile> getListUploadFile() {
		return listUploadFile;
	}

	public void setListUploadFile(List<UploadFile> listUploadFile) {
		this.listUploadFile = listUploadFile;
	}

}
