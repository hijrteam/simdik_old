package id.co.hijr.app.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.app.core.BaseController;
import id.co.hijr.app.core.Pagination;
import id.co.hijr.app.core.ResponseWrapper;
import id.co.hijr.app.domain.MaritalStatus;
import id.co.hijr.app.domain.Profile;
import id.co.hijr.app.service.AppManagerService;

@Scope("session")
@RestController
@RequestMapping("/profile/")
public class ProfileRestService extends BaseController implements Serializable {

	@Autowired
	AppManagerService appManagerService  ;
	
	Profile profile;
	List<Profile> listProfile = new ArrayList<Profile>();
	
	public ProfileRestService() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper list(@RequestParam String username, @RequestBody ProfileRestService req) {	
		ResponseWrapper resp = new ResponseWrapper();
		// get pagination properties from request if profile change their pagination
		Pagination p = req.getPagination();
		p.setRecordcount(appManagerService.getProfileDao().count(p.getFilter()));
		
		List<Profile> data = appManagerService.getProfileDao().list(p.getFilter(), p.getIndex(), p.getRowcount());
		resp.setData(data);
		resp.setPagination(p);
		return resp;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper delete(@RequestParam String username, @RequestBody ProfileRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			appManagerService.removeProfile(req.getPagination().getFilter(),"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper save(@RequestParam String username, @RequestBody ProfileRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			
			if(req.getListProfile().size() == 0)
				req.getListProfile().add(req.getProfile());
			appManagerService.saveProfile(req.getListProfile(),"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}
	

	

	public Profile getProfile() {
		return profile;
	}
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<Profile> getListProfile() {
		return listProfile;
	}

	public void setListProfile(List<Profile> listProfile) {
		this.listProfile = listProfile;
	}

}
