package id.co.hijr.app.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.hijr.app.core.BaseController;
import id.co.hijr.app.core.Clause;
import id.co.hijr.app.core.Pagination;
import id.co.hijr.app.core.ResponseWrapper;
import id.co.hijr.app.domain.Role;
import id.co.hijr.app.domain.User;
import id.co.hijr.app.service.AppManagerService;

@Scope("session")
@RestController
@RequestMapping("/user/")
public class UserRestService extends BaseController implements Serializable {

	@Autowired
	AppManagerService appManagerService  ;
	
	User user;
	List<User> listUser = new ArrayList<User>();
	
	public UserRestService() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper list(@RequestParam String username, @RequestBody UserRestService req) {	
		ResponseWrapper resp = new ResponseWrapper();
		// get pagination properties from request if user change their pagination
		Pagination p = req.getPagination();
		p.setRecordcount(appManagerService.getUserDao().count(p.getFilter()));
		
		List<User> data = appManagerService.getUserDao().list(p.getFilter(), p.getIndex(), p.getRowcount());
		resp.setData(data);
		resp.setPagination(p);
		return resp;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper delete(@RequestParam String username, @RequestBody UserRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			appManagerService.removeUser(req.getPagination().getFilter(),"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, headers={"Accept=application/json","Content-type=application/json"})
	public ResponseWrapper save(@RequestParam String username, @RequestBody UserRestService req) {
		ResponseWrapper resp = new ResponseWrapper();
		try {
			
			if(req.getListUser().size() == 0)
				req.getListUser().add(req.getUser());
			appManagerService.saveUser(req.getListUser(),"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			resp.setMessage(e.getMessage());
			resp.setStatus(ResponseWrapper.STATUS_ERROR);
		}
		return resp;
	}
	

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getListUser() {
		return listUser;
	}

	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}

}
