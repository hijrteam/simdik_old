package id.co.hijr.app.test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import id.co.hijr.app.core.Clause;
import id.co.hijr.app.domain.Role;
import id.co.hijr.app.domain.User;
import id.co.hijr.app.service.AppManagerService;

public class ServiceTest {

	
	
	public ServiceTest(){
		
		String configLocation="/Web/WEB-INF/applicationContext.xml";
		ApplicationContext ctx = new FileSystemXmlApplicationContext(configLocation);
		
		AppManagerService svc = ctx.getBean(AppManagerService.class);
		
		User loginUser = new User(1);
		loginUser.setRole(new Role(1));
		
		
		List<Clause> filter = new ArrayList<>();
		filter.add(new Clause("setting_code", "in", "('APP_NAME','APP_VER')"));
		
		System.out.println(svc.getSettingDao().list(filter));
	}
	
	
	public static void main(String[] args) {
		new ServiceTest();
	}

}
