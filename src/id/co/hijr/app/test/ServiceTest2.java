package id.co.hijr.app.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import id.co.hijr.app.core.Clause;
import id.co.hijr.app.domain.Agama;
import id.co.hijr.app.domain.Institusi;
import id.co.hijr.app.domain.JenisRuangan;
import id.co.hijr.app.domain.Jenjang;
import id.co.hijr.app.domain.Murid;
import id.co.hijr.app.domain.Role;
import id.co.hijr.app.domain.Ruangan;
import id.co.hijr.app.domain.Tingkatan;
import id.co.hijr.app.domain.User;
import id.co.hijr.app.service.AppManagerService;

public class ServiceTest2 {

	
	
	public ServiceTest2() throws Exception{
		
		String configLocation="/Web/WEB-INF/applicationContext.xml";
		ApplicationContext ctx = new FileSystemXmlApplicationContext(configLocation);
		
		AppManagerService svc = ctx.getBean(AppManagerService.class);
		
		Murid o = new Murid();
		o.setNamaLengkap("Kayum Munajir");
		o.setJenisKelamin(1);
		o.setTempatLahir("SIRIWINI");
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		o.setTanggalLahir(date);
		Agama a = new Agama(1);
		o.setAgama(a);
		Tingkatan t = new Tingkatan(1);
		o.setTingkatan(t);
		//svc.getMuridDao().add(o);
		//svc.getJenjangDao().remove(2);
		List<Murid> listMurid= new ArrayList<Murid>();
		listMurid.add(svc.getMuridDao().get(1));
		
		for(Murid o1 : listMurid) {
			System.out.println(o1.getId() + "-" + o.getNamaLengkap() + "-" + o.tempatLahir);			
		}
		
		JenisRuangan oA = new JenisRuangan();
		oA.setNamaJenisRuangan("PERPUSTAKAAN");
		//oA.setSubRuangan("A");
		svc.getJenisRuanganDao().add(oA);
		
		List<JenisRuangan> listJenisRuangan = new ArrayList<JenisRuangan>();
		listJenisRuangan.add(svc.getJenisRuanganDao().get(1));
		
		for(JenisRuangan o1 : listJenisRuangan) {
			System.out.println(o1.getId() + "-" + o1.getNamaJenisRuangan());
		}
	}
	
	
	public static void main(String[] args) throws Exception {
		new ServiceTest2();
	}

}
