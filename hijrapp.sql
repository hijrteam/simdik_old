# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.1.44)
# Database: hijrapp
# Generation Time: 2016-03-07 14:27:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_marital_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_marital_status`;

CREATE TABLE `tbl_marital_status` (
  `marital_status_id` int(11) unsigned NOT NULL,
  `marital_status_name` varchar(15) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_added` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_modified` varchar(50) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `user_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_marital_status` WRITE;
/*!40000 ALTER TABLE `tbl_marital_status` DISABLE KEYS */;

INSERT INTO `tbl_marital_status` (`marital_status_id`, `marital_status_name`, `date_added`, `user_added`, `date_modified`, `user_modified`, `date_deleted`, `user_deleted`)
VALUES
	(1,'Kawin',NULL,NULL,NULL,NULL,NULL,NULL),
	(2,'Belum Kawin',NULL,NULL,NULL,NULL,NULL,NULL),
	(3,'Duda / Janda',NULL,NULL,NULL,NULL,NULL,NULL),
	(4,'Cerai',NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_marital_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_month
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_month`;

CREATE TABLE `tbl_month` (
  `month_id` int(11) unsigned NOT NULL,
  `month_code` varchar(5) NOT NULL DEFAULT '',
  `month_name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`month_id`),
  UNIQUE KEY `BULAN_UNQ` (`month_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_month` WRITE;
/*!40000 ALTER TABLE `tbl_month` DISABLE KEYS */;

INSERT INTO `tbl_month` (`month_id`, `month_code`, `month_name`)
VALUES
	(1,'01','Januari'),
	(2,'02','Februari'),
	(3,'03','Maret'),
	(4,'04','April'),
	(5,'05','Mei'),
	(6,'06','Juni'),
	(7,'07','Juli'),
	(8,'08','Agustus'),
	(9,'09','September'),
	(10,'10','Oktober'),
	(11,'11','November'),
	(12,'12','Desember');

/*!40000 ALTER TABLE `tbl_month` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_profile`;

CREATE TABLE `tbl_profile` (
  `profile_id` int(11) unsigned NOT NULL,
  `profile_user` int(11) DEFAULT NULL,
  `profile_picture` int(11) DEFAULT NULL,
  `home_phone_number` varchar(20) DEFAULT NULL,
  `home_address` varchar(100) DEFAULT NULL,
  `home_city` varchar(20) DEFAULT NULL,
  `home_province` int(11) DEFAULT NULL,
  `home_postal_code` varchar(10) DEFAULT NULL,
  `mobile_phone` varchar(20) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `marital_status` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_added` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_modified` varchar(50) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `user_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_profile` WRITE;
/*!40000 ALTER TABLE `tbl_profile` DISABLE KEYS */;

INSERT INTO `tbl_profile` (`profile_id`, `profile_user`, `profile_picture`, `home_phone_number`, `home_address`, `home_city`, `home_province`, `home_postal_code`, `mobile_phone`, `birth_date`, `gender`, `marital_status`, `date_added`, `user_added`, `date_modified`, `user_modified`, `date_deleted`, `user_deleted`)
VALUES
	(1,1,10,'0251','Cimanggu','Bogor',8,'16113','081281823698','1984-08-13',1,1,'2016-03-07 21:45:12','','2016-03-07 21:54:14','',NULL,NULL);

/*!40000 ALTER TABLE `tbl_profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_province
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_province`;

CREATE TABLE `tbl_province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `province_code` varchar(100) DEFAULT NULL,
  `province_name` varchar(200) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_added` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_modified` varchar(50) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `user_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_province` WRITE;
/*!40000 ALTER TABLE `tbl_province` DISABLE KEYS */;

INSERT INTO `tbl_province` (`province_id`, `province_code`, `province_name`, `date_added`, `user_added`, `date_modified`, `user_modified`, `date_deleted`, `user_deleted`)
VALUES
	(1,'01','Aceh',NULL,NULL,NULL,NULL,NULL,NULL),
	(2,'02','Bali',NULL,NULL,NULL,NULL,NULL,NULL),
	(3,'03','Banten',NULL,NULL,NULL,NULL,NULL,NULL),
	(4,'04','Bengkulu',NULL,NULL,NULL,NULL,NULL,NULL),
	(5,'05','Gorontalo',NULL,NULL,NULL,NULL,NULL,NULL),
	(6,'06','Jakarta',NULL,NULL,NULL,NULL,NULL,NULL),
	(7,'07','Jambi',NULL,NULL,NULL,NULL,NULL,NULL),
	(8,'08','Jawa Barat',NULL,NULL,NULL,NULL,NULL,NULL),
	(9,'09','Jawa Tengah',NULL,NULL,NULL,NULL,NULL,NULL),
	(10,'10','Jawa Timur',NULL,NULL,NULL,NULL,NULL,NULL),
	(11,'11','Kalimantan Barat',NULL,NULL,NULL,NULL,NULL,NULL),
	(12,'12','Kalimantan Selatan',NULL,NULL,NULL,NULL,NULL,NULL),
	(13,'13','Kalimantan Tengah',NULL,NULL,NULL,NULL,NULL,NULL),
	(14,'14','Kalimantan Timur',NULL,NULL,NULL,NULL,NULL,NULL),
	(15,'15','Kalimantan Utara',NULL,NULL,NULL,NULL,NULL,NULL),
	(16,'16','Kepulauan Bangka Belitung',NULL,NULL,NULL,NULL,NULL,NULL),
	(17,'17','Kepulauan Riau',NULL,NULL,NULL,NULL,NULL,NULL),
	(18,'18','Lampung',NULL,NULL,NULL,NULL,NULL,NULL),
	(19,'19','Maluku',NULL,NULL,NULL,NULL,NULL,NULL),
	(20,'20','Maluku Utara',NULL,NULL,NULL,NULL,NULL,NULL),
	(21,'21','Nusa Tenggara Barat',NULL,NULL,NULL,NULL,NULL,NULL),
	(22,'22','Nusa Tenggara Timur',NULL,NULL,NULL,NULL,NULL,NULL),
	(23,'23','Papua',NULL,NULL,NULL,NULL,NULL,NULL),
	(24,'24','Papua Barat',NULL,NULL,NULL,NULL,NULL,NULL),
	(25,'25','Riau',NULL,NULL,NULL,NULL,NULL,NULL),
	(26,'26','Sulawesi Barat',NULL,NULL,NULL,NULL,NULL,NULL),
	(27,'27','Sulawesi Selatan',NULL,NULL,NULL,NULL,NULL,NULL),
	(28,'28','Sulawesi Tengah',NULL,NULL,NULL,NULL,NULL,NULL),
	(29,'29','Sulawesi Tenggara',NULL,NULL,NULL,NULL,NULL,NULL),
	(30,'30','Sulawesi Utara',NULL,NULL,NULL,NULL,NULL,NULL),
	(31,'31','Sumatera Barat',NULL,NULL,NULL,NULL,NULL,NULL),
	(32,'32','Sumatera Selatan',NULL,NULL,NULL,NULL,NULL,NULL),
	(33,'33','Sumatera Utara',NULL,NULL,NULL,NULL,NULL,NULL),
	(34,'34','Yogyakarta',NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_province` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_role`;

CREATE TABLE `tbl_role` (
  `role_id` int(11) NOT NULL,
  `name_role` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `date_added` date DEFAULT NULL,
  `user_added` varchar(50) DEFAULT NULL,
  `date_modified` date DEFAULT NULL,
  `user_modified` varchar(50) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `user_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_role` WRITE;
/*!40000 ALTER TABLE `tbl_role` DISABLE KEYS */;

INSERT INTO `tbl_role` (`role_id`, `name_role`, `description`, `date_added`, `user_added`, `date_modified`, `user_modified`, `date_deleted`, `user_deleted`)
VALUES
	(1,'admin','Full Access',NULL,NULL,NULL,NULL,NULL,NULL),
	(2,'guest','View Only',NULL,NULL,NULL,NULL,NULL,NULL),
	(3,'rest','Restful Access',NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_role_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_role_map`;

CREATE TABLE `tbl_role_map` (
  `role_name` varchar(50) NOT NULL DEFAULT '',
  `end_point` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_name`,`end_point`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_role_map` WRITE;
/*!40000 ALTER TABLE `tbl_role_map` DISABLE KEYS */;

INSERT INTO `tbl_role_map` (`role_name`, `end_point`)
VALUES
	('admin','/user/');

/*!40000 ALTER TABLE `tbl_role_map` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_setting`;

CREATE TABLE `tbl_setting` (
  `setting_id` int(11) unsigned NOT NULL,
  `setting_code` varchar(50) NOT NULL DEFAULT '',
  `setting_name` varchar(50) NOT NULL DEFAULT '',
  `setting_value` varchar(50) NOT NULL DEFAULT '',
  `setting_category` int(11) NOT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `UNIKEY` (`setting_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_setting` WRITE;
/*!40000 ALTER TABLE `tbl_setting` DISABLE KEYS */;

INSERT INTO `tbl_setting` (`setting_id`, `setting_code`, `setting_name`, `setting_value`, `setting_category`)
VALUES
	(10,'APP_NAME','Application Name','Application Development Framework',3),
	(11,'APP_VER','Version','1.0',3),
	(3,'SMTP_URL','Email Server','http://mail.google.com',2),
	(4,'SMTP_USER','Email User ID','hijrcorp',2),
	(5,'SMTP_PASS','Email Password','hijrpass',2),
	(6,'SMS_GATEWAY','Sms Center','http://www.sms.com',2),
	(7,'DEF_LOGIN_TIMEOUT_DAY','Default Login Timeout (day)','7',1),
	(8,'COPYRIGHT_YEAR','Year of Copyright','2016',0),
	(9,'APP_CODE','Application Code','HIJRAPP',3);

/*!40000 ALTER TABLE `tbl_setting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_setting_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_setting_category`;

CREATE TABLE `tbl_setting_category` (
  `setcat_id` int(11) unsigned NOT NULL,
  `setcat_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`setcat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_setting_category` WRITE;
/*!40000 ALTER TABLE `tbl_setting_category` DISABLE KEYS */;

INSERT INTO `tbl_setting_category` (`setcat_id`, `setcat_name`)
VALUES
	(1,'General'),
	(2,'Messaging'),
	(3,'System');

/*!40000 ALTER TABLE `tbl_setting_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_upload_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_upload_file`;

CREATE TABLE `tbl_upload_file` (
  `upload_file_id` bigint(11) unsigned NOT NULL,
  `upload_file_name` varchar(500) NOT NULL DEFAULT '',
  `upload_file_size` bigint(11) NOT NULL,
  `table_folder` varchar(50) NOT NULL DEFAULT '',
  `upload_file_type` varchar(50) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_added` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_modified` varchar(50) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `user_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`upload_file_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_upload_file` WRITE;
/*!40000 ALTER TABLE `tbl_upload_file` DISABLE KEYS */;

INSERT INTO `tbl_upload_file` (`upload_file_id`, `upload_file_name`, `upload_file_size`, `table_folder`, `upload_file_type`, `date_added`, `user_added`, `date_modified`, `user_modified`, `date_deleted`, `user_deleted`)
VALUES
	(1,'test.png',6404,'payment','image/png','2016-01-14 21:06:22','admin',NULL,NULL,NULL,NULL),
	(2,'test.png',6404,'payment','image/png','2016-01-14 21:18:37','admin',NULL,NULL,NULL,NULL),
	(3,'persetujuan naskah ringkas.pdf',14754,'payment','\"application/pdf\"','2016-01-14 21:19:07','admin',NULL,NULL,NULL,NULL),
	(4,'test.png',6404,'payment','image/png','2016-01-14 21:25:02','admin',NULL,NULL,NULL,NULL),
	(5,'test.png',6404,'payment','image/png','2016-01-14 21:25:33','admin',NULL,NULL,NULL,NULL),
	(6,'test.png',6404,'payment','image/png','2016-01-14 21:26:33','admin',NULL,NULL,NULL,NULL),
	(7,'test.png',6404,'payment','image/png','2016-01-14 21:26:43','admin',NULL,NULL,NULL,NULL),
	(8,'test.png',6404,'payment','image/png','2016-01-14 21:27:45','admin',NULL,NULL,NULL,NULL),
	(9,'test.png',6404,'payment','image/png','2016-01-14 21:27:54','admin',NULL,NULL,NULL,NULL),
	(10,'cars.png',516,'payment','image/png','2016-03-07 21:54:06','',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_upload_file` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `real_name` varchar(50) NOT NULL DEFAULT '',
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `description` varchar(200) DEFAULT '',
  `email` varchar(200) DEFAULT NULL,
  `token` varchar(100) DEFAULT '',
  `expired` datetime DEFAULT NULL,
  `role` tinyint(1) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `user_added` varchar(50) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `user_modified` varchar(50) DEFAULT NULL,
  `date_deleted` datetime DEFAULT NULL,
  `user_deleted` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UNIQUE` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;

INSERT INTO `tbl_user` (`user_id`, `real_name`, `username`, `password`, `description`, `email`, `token`, `expired`, `role`, `date_added`, `user_added`, `date_modified`, `user_modified`, `date_deleted`, `user_deleted`)
VALUES
	(1,'Administrator','admin','21232f297a57a5a743894a0e4a801fc3','Predefined user','admin@company.com','c1036ff8519b88b01685af766bf5c434','2016-03-14 22:17:07',1,'2000-01-01 00:00:00','allah','2016-03-07 22:17:07','admin',NULL,NULL),
	(2,'Rest Client','restclient','','','',NULL,NULL,3,'2000-01-01 00:00:00','admin','2016-03-02 16:34:53','admin',NULL,NULL),
	(3,'Guest','user','202cb962ac59075b964b07152d234b70','view only','guest@domain.com','3810cd667f34c5591266ec72a122dd3','2016-03-13 17:04:09',2,'2016-02-03 17:52:02','admin','2016-03-06 17:04:09','admin',NULL,NULL),
	(4,'a','a','a','a','a@a.com',NULL,NULL,1,'2016-03-02 17:37:38','admin','2016-03-02 18:21:19','admin',NULL,NULL);

/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_user_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_user_log`;

CREATE TABLE `tbl_user_log` (
  `user_name` varchar(100) NOT NULL DEFAULT '',
  `log_time` datetime NOT NULL,
  `log_status` varchar(50) DEFAULT NULL,
  `log_action` varchar(50) DEFAULT NULL,
  `log_resources` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_user_log` WRITE;
/*!40000 ALTER TABLE `tbl_user_log` DISABLE KEYS */;

INSERT INTO `tbl_user_log` (`user_name`, `log_time`, `log_status`, `log_action`, `log_resources`)
VALUES
	('admin','2016-03-02 17:32:12','success','login',''),
	('admin','2016-03-02 17:37:38','success','add','user'),
	('admin','2016-03-02 17:48:49','success','add','user'),
	('admin','2016-03-02 17:54:13','success','add','user'),
	('admin','2016-03-02 18:21:19','success','update','user'),
	('admin','2016-03-02 19:43:13','success','logout',''),
	('admin','2016-03-02 20:07:34','success','login',''),
	('admin','2016-03-02 20:16:07','success','update','user'),
	('admin','2016-03-02 20:16:11','success','logout',''),
	('user','2016-03-02 20:16:14','success','login',''),
	('user','2016-03-02 20:17:14','success','update','user'),
	('user','2016-03-02 20:17:27','failed','update','password'),
	('user','2016-03-02 20:17:42','success','update','password'),
	('user','2016-03-02 20:17:50','success','logout',''),
	('admin','2016-03-02 20:17:53','success','login',''),
	('admin','2016-03-06 17:03:22','success','login',''),
	('admin','2016-03-06 17:03:31','success','list','user'),
	('guest','2016-03-06 17:03:51','failed','login',''),
	('guest','2016-03-06 17:03:56','failed','login',''),
	('admin','2016-03-06 17:04:02','success','update','user'),
	('user','2016-03-06 17:04:09','success','login',''),
	('user','2016-03-06 17:04:17','success','list','user'),
	('admin','2016-03-06 17:06:06','success','list','user'),
	('user','2016-03-06 17:06:10','success','list','user'),
	('user','2016-03-06 17:06:11','success','list','user'),
	('user','2016-03-06 17:06:12','success','list','user'),
	('user','2016-03-06 17:06:12','success','list','user'),
	('user','2016-03-06 17:06:12','success','list','user'),
	('user','2016-03-06 17:06:13','success','list','user'),
	('user','2016-03-06 17:06:13','success','list','user'),
	('user','2016-03-06 17:06:13','success','list','user'),
	('user','2016-03-06 17:06:13','success','list','user'),
	('user','2016-03-06 17:06:13','success','list','user'),
	('user','2016-03-06 17:06:13','success','list','user'),
	('user','2016-03-06 17:06:14','success','list','user'),
	('user','2016-03-06 17:06:14','success','list','user'),
	('admin','2016-03-06 17:45:34','success','logout',''),
	('admin','2016-03-06 17:48:25','success','login',''),
	('admin','2016-03-06 17:48:41','failed','update','password'),
	('admin','2016-03-06 17:48:47','success','update','password'),
	('admin','2016-03-06 17:49:29','success','logout',''),
	('admin','2016-03-07 10:54:57','success','login',''),
	('admin','2016-03-07 21:36:42','success','login',''),
	('admin','2016-03-07 22:17:07','success','login','');

/*!40000 ALTER TABLE `tbl_user_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_user_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_user_setting`;

CREATE TABLE `tbl_user_setting` (
  `userset_id` int(11) unsigned NOT NULL,
  `userset_code` varchar(50) NOT NULL DEFAULT '',
  `userset_name` varchar(50) NOT NULL DEFAULT '',
  `userset_value` varchar(50) NOT NULL DEFAULT '',
  `userset_cateogry` int(11) NOT NULL,
  PRIMARY KEY (`userset_id`),
  UNIQUE KEY `UNIKEY` (`userset_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
